<!DOCTYPE html>
<html lang="en">
  <head>
    <title>UNT Voter Booth - Profiles</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">
    
    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="css/flat-ui.css" rel="stylesheet">
    <link href="css/template.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
	  
	  <style>
		  .img-circle{
			-moz-box-shadow: 0px 0px 0px 5px #e8e6e6; /* Firefox */  
		  	-webkit-box-shadow: 0px 0px 0px 5px #e8e6e6; /* Safari, Chrome */  
		 	box-shadow: 0px 0px 0px 5px #e8e6e6; /* CSS3 */
		  }
		  
		  label{
			  cursor: pointer;
		  }
	  </style>
  </head>

  <body>


    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
            <li><a href="#">Vote for an Election</a></li>
            <li><a href="#">View/Edit my Profile</a></li>
            <li><a href="#">Browse Profiles</a></li>
			<li><a href="#">View Results</a></li>
        </ul>
    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="images/sidemenuicon.svg" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                 <!--       <div id="logo"></div>    --> 
                    </div>
					
					<!-- User account drop down. Only visible when logged in -->
					<div id="account-button" class="hidden col-xs-2">
						<div class="dropdown pull-right">
					    	<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['name']; ?><span class="caret"></span></button>
					  		<span class="dropdown-arrow"></span>
					  		<ul class="dropdown-menu">
								<li><a href="account.html">Account</a></li>
								<li><a href="my-posts.php">My posts</a></li>
								<li><a href="logout.php">Logout</a></li>
					  		</ul>
						</div>
					</div>
					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(images/home-bg.jpg);">
                        <h1 class="uppercase" id="banner-text">Profiles</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->        
        <div class="container">
            <div class="row" id="content">
					<br>
                    <div class="col-xs-12 col-md-8 div-center">
						<div class ="row text-center">
							<h3>Full Name</h3>
						</div>
						<br>
						<div class ="row">
							<div class="col-xs-12 div-center text-center"> 
								
								<strong class="uppercase">College</strong>
								<p>
								College of Engineering
								</p>
								
								
								<strong class="uppercase">Major</strong>
								<p>
								Computer Science
								</p>
								
								
								<strong class="uppercase">Classification</strong>
								<p>
								Junior
								</p>
								
								<strong class="uppercase">Bio</strong>
								<p>
								Sample bio of student.
								</p>
								
								<strong class="uppercase">Previously Held Positions</strong>
								<p>
								N/A
								</p>
								
							</div>
						</div>
						<br>
					</div>
					
        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made at the University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jasny-bootstrap.min.js"></script>
	<script src="js/jquery.tagsinput.js"></script>
	  <script src="js/flatui-radio.js"></script>
	<script type='text/javascript'>
		$(".tagsinput").tagsInput();
	</script>
  </body>
</html>