<!DOCTYPE html>
<html lang="en">
  <head>
    <title>UNT Voter Booth - Create Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">
    
    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="css/flat-ui.css" rel="stylesheet">
    <link href="css/template.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
	  
	  <style>
		  .img-circle{
			-moz-box-shadow: 0px 0px 0px 5px #e8e6e6; /* Firefox */  
		  	-webkit-box-shadow: 0px 0px 0px 5px #e8e6e6; /* Safari, Chrome */  
		 	box-shadow: 0px 0px 0px 5px #e8e6e6; /* CSS3 */
		  }
		  
		  label{
			  cursor: pointer;
		  }
	  </style>
  </head>

  <body>


    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
<li><a href="CandiElections.php">Vote for an Election</a></li>
<li><a href="ViewProfile.php">View my Profile</a></li>
<li><a href="EditProfile.php">Edit my Profile</a></li>
<li><a href="profilesCandi.php">Browse Profiles</a></li>
<li><a href="CandiElectionResults.php">View Results</a></li>
        </ul>
    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="images/sidemenuicon.svg" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                 <!--       <div id="logo"></div>    --> 
                    </div>
					
					<!-- User account drop down. Only visible when logged in -->
					<div id="account-button" class="hidden col-xs-2">
						<div class="dropdown pull-right">
					    	<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['name']; ?><span class="caret"></span></button>
					  		<span class="dropdown-arrow"></span>
					  		<ul class="dropdown-menu">
								<li><a href="account.html">Account</a></li>
								<li><a href="my-posts.php">My posts</a></li>
								<li><a href="logout.php">Logout</a></li>
					  		</ul>
						</div>
					</div>
					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(images/home-bg.jpg);">
                        <h1 class="uppercase" id="banner-text">Create Profile</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->
		<form role="form" name="profileform" method="post" action="create-profile.php">
        <div class="container">
            <div class="row" id="content">
					<br>
                    <div class="col-xs-12 col-md-8 div-center">
						<br>
						<div class ="row">
							<div class="col-xs-12"> 
								
								<div class="row">
									<div class="col-xs-12">
										<strong class="uppercase">Full Name</strong>
										<input name="FullName" type="text" class="form-control" placeholder="Your full name">
										<br>
									</div>
								</div>
								<div class="row">
									
									<div class="col-xs-12 col-sm-3">
										<strong class="uppercase">College</strong>	
											<div class="dropdown">
										  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
										    College
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Arts and Sciences</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Business</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Education</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Engineering</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Honors College</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Information</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Frank W. and Sue Mayborn School of Journalism</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Merchandising, Hospitality and Tourism</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Music</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Public Affairs and Community Service</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Visual Arts and Design</a></li>
										  </ul>
										</div>
										<br>
								</div>
							
								
								<div class="col-xs-12 col-sm-4">
										<strong class="uppercase">Department</strong>	
											<div class="dropdown">
										  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
										    Department
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Math</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Physics</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Computer Science</a></li>
										
										  </ul>
										</div>
										<br>
								</div>
								
								<div class="col-xs-12 col-sm-5">
										<strong class="uppercase">Position Running For</strong>	
											<div class="dropdown">
										  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
										    Position
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">President</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Secretary</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Tresurer</a></li>
										
										  </ul>
										</div>
										<br>
								</div>
								
								</div>
								<strong class="uppercase">Classification</strong>
								<div class="row">
									<div class="col-xs-12">
										<label class="radio col-sm-3">
										  <input type="radio" name="group1" value="Freshman" data-toggle="radio">
										  Freshman
										</label>
										<label class="radio col-sm-3">
										  <input type="radio" name="group1" value="Sophomore" data-toggle="radio">
										  Sophomore
										</label>
										<label class="radio col-sm-2">
										  <input type="radio" name="group1" value="Junior" data-toggle="radio">
										  Junior
										</label>
										<label class="radio col-sm-2">
										  <input type="radio" name="group1" value="Senior" data-toggle="radio">
										  Senior
										</label>
										<label class="radio col-sm-2">
										  <input type="radio" name="group1" value="Graduate" data-toggle="radio">
										  Graduate
										</label>
									</div>
								</div>	
									
								<br>
								
								<strong class="uppercase">Bio</strong>
								<textarea class="form-control" id="textDescription" name="textDescription" rows="5" placeholder="Give a brief bio about yourself"></textarea>
								<script>
										$('#textDescription').val();
										
								</script>
								<br>
								
								<strong class="uppercase">Previously Held Positions</strong>
								<input type="text" class="form-control" name="previouspositions" placeholder="Previous positions" required/>
								<br>
								
							</div>
						</div>
						<br>
					</div>
				<div class="div-center text-center">
					<button class="btn btn-info btn-lg" id="butn">Submit</button>
				</form>
				</div>
            </div>
        </div>

        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made at the University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jasny-bootstrap.min.js"></script>
	<script src="js/jquery.tagsinput.js"></script>
	  <script src="js/flatui-radio.js"></script>
	<script type='text/javascript'>
		$(".tagsinput").tagsInput();
	</script>
  </body>
</html>