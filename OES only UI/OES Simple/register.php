<!DOCTYPE html>
<html lang="en">
  <head>
    <title>UNT Voter Booth - Create an Account</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">
    
    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="css/flat-ui.css" rel="stylesheet">
    <link href="css/template.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

 
    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
            <li><a href="login.php">Home</a></li>
        </ul>
        <br/>
    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="images/sidemenuicon.svg" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
           <!--           <div id="logo"></div>   -->   
                    </div>
					
					<!-- User account drop down. Only visible when logged in -->
					<div id="account-button" class="hidden col-xs-2">
                        <div class="dropdown pull-right">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['name']; ?><span class="caret"></span></button>
                            <span class="dropdown-arrow"></span>
                            <ul class="dropdown-menu">
                                <li><a href="account.html">Account</a></li>
                                <li><a href="my-posts.php">My posts</a></li>
                                <li><a href="logout.php" id="logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(images/home-bg.jpg);">
                        <h1 class="uppercase" id="banner-text">Create an Account</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->
        <div class="container">
            <div class="row">
				<br/><br/>
				<div class="col-xs-12 col-sm-7 col-lg-5 div-center">
					<form action="signup.php" method="post" class="well well-lg text-center"  role="form">
						
						<label><strong class="uppercase"> Full Name</strong></label>
						<div class="form-group row col-sm-10 div-center">
                            <div class="input-group">
								<span class="input-group-addon"><span class="input-icon fui-user"></span></span>
								<input type="text" class="form-control" name="fullName" placeholder="Full name" required>
							</div>							
						</div>

						<br/>
						
						<label><strong class="uppercase">UNT Email Address</strong></label>
						<div class="form-group col-sm-10 div-center">
							<div class="input-group">
								<span class="input-group-addon"><span class="input-icon fui-mail"></span></span>
								<input type="email" class="form-control" name="emailAddress" placeholder="Your UNT email address" required>
							</div>
						</div>
						
						<br/>
						
						<label><strong class="uppercase">UNT ID</strong></label>
						<div class="form-group col-sm-10 div-center">
							<div class="input-group">
								<span class="input-group-addon"><span class="input-icon fui-user"></span></span>
								<input type="email" class="form-control" name="emailAddress" placeholder="Your UNT ID number" required>
							</div>
						</div>
						
						<br/>
						<label><strong class="uppercase">Password</strong></label>
						<div class="form-group col-sm-10 div-center">
							<div class="input-group">
								<span class="input-group-addon"><span class="input-icon fui-lock"></span></span>
								<input type="password" class="form-control" name="password" placeholder="Minimum 8 characters" required>
							</div>
						</div>
						
						<br/>
						
						<label><strong class="uppercase">Student/Faculty</strong></label>
									<div class="form-group col-sm-10 div-center">
										<div class="dropdown">
										  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
										    Student/Faculty
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu2">
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Student</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Faculty</a></li>
										  </ul>
										</div>
									</div>
								<br/>
										
							<label><strong class="uppercase">College</strong></label>
											<div class="dropdown">
										  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
										    College
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Arts and Sciences</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Business</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Education</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Engineering</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Honors College</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Information</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Frank W. and Sue Mayborn School of Journalism</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Merchandising, Hospitality and Tourism</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Music</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Public Affairs and Community Service</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">College of Visual Arts and Design</a></li>
										  </ul>
										</div>
									<br/>
									
							<label><strong class="uppercase">Department</strong></label>	
											<div class="dropdown">
										  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
										    Department
										    <span class="caret"></span>
										  </button>
										  <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Math</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Physics</a></li>
										    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Computer Science</a></li>
										
										  </ul>
										</div>
									<br/>

						<div class="form-actions">
					  		<button type="submit" class="btn btn-info">Create my account</button>
						</div>
					</form>	
				</div>
                <br/>
            </div>
        </div>

        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made at the University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jasny-bootstrap.min.js"></script>

  </body>

</html>