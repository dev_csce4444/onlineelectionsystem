-- phpMyAdmin SQL Dump
-- version 4.1.14.6
-- http://www.phpmyadmin.net
--
-- Host: db547447633.db.1and1.com
-- Generation Time: Nov 05, 2014 at 06:09 PM
-- Server version: 5.1.73-log
-- PHP Version: 5.4.4-14+deb7u14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db547447633`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `email` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `head_admin` tinyint(1) NOT NULL DEFAULT '0',
  `admin_approved` tinyint(1) NOT NULL DEFAULT '0',
  `monitor_election` tinyint(1) NOT NULL DEFAULT '0',
  `add_candidate` tinyint(1) NOT NULL DEFAULT '0',
  `create_election` tinyint(1) NOT NULL DEFAULT '0',
  `remove_election` tinyint(1) NOT NULL DEFAULT '0',
  `remove_candidate` tinyint(1) NOT NULL DEFAULT '0',
  `approve_registration` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
