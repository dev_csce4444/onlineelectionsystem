-- phpMyAdmin SQL Dump
-- version 4.1.14.6
-- http://www.phpmyadmin.net
--
-- Host: db547447633.db.1and1.com
-- Generation Time: Nov 05, 2014 at 06:10 PM
-- Server version: 5.1.73-log
-- PHP Version: 5.4.4-14+deb7u14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db547447633`
--

-- --------------------------------------------------------

--
-- Table structure for table `elections`
--

CREATE TABLE IF NOT EXISTS `elections` (
  `position` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `feedback` varchar(500) COLLATE latin1_general_ci NOT NULL,
  `eligibility` varchar(300) COLLATE latin1_general_ci NOT NULL,
  `registration_start` datetime DEFAULT NULL,
  `registration_over` datetime DEFAULT NULL,
  `election_start` datetime DEFAULT NULL,
  `election_over` datetime DEFAULT NULL,
  `reminder_sent_date` datetime DEFAULT NULL COMMENT 'last time reminder was sent',
  `reelection_requested` tinyint(1) NOT NULL DEFAULT '0',
  `final_result` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
