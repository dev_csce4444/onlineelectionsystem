-- phpMyAdmin SQL Dump
-- version 4.1.14.6
-- http://www.phpmyadmin.net
--
-- Host: db547447633.db.1and1.com
-- Generation Time: Nov 05, 2014 at 06:10 PM
-- Server version: 5.1.73-log
-- PHP Version: 5.4.4-14+deb7u14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db547447633`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `email` varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `first_name` varchar(15) COLLATE latin1_general_ci NOT NULL,
  `last_name` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `untID` int(8) DEFAULT NULL,
  `faculty_student` varchar(8) COLLATE latin1_general_ci NOT NULL,
  `college` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `department` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `major` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `year` varchar(15) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `untID` (`untID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
