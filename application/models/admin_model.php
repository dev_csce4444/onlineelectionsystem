<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {

    //returns the email for all current admins in an array
    // use this to display all the current admins
// also returns just general information about the users
    public function get_all_admins() {
        $sql = "SELECT a.email,a.admin_approved,u.first_name,u.last_name,u.faculty_student,u.college,u.major,u.year FROM admins a NATURAL JOIN users u";
        $query = $this->db->query($sql);

        $result = array();
        foreach ($query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name;
            $result['last_name'][] = $row->last_name;
            $result['admin_approved'][] = $row->admin_approved;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['major'][] = $row->major;
            $result['year'][] = $row->year;
        }
        return $result;
    }

    public function get_approved_admins() {
        $sql = "SELECT a.email,u.first_name,u.last_name,u.faculty_student,u.college,u.major,u.year FROM admins a NATURAL JOIN users u WHERE a.admin_approved = 1";
        $query = $this->db->query($sql);

        $result = array();
        foreach ($query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name;
            $result['last_name'][] = $row->last_name;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['major'][] = $row->major;
            $result['year'][] = $row->year;
        }
        return $result;
    }

    // check to see if the specific email is an admin email account or not
    // use to choose what version of the website to display
    public function is_user_an_admin($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

        $this->db->where('email', $email); //prepare the sql statement
        $result = $this->db->get('admins'); //pick the table to select form        
        if ($result->num_rows() == 1)
            return TRUE; //email is an admin
        else
            return FALSE;
    }

    // check to see if the specific email is an admin email account or not
    // use to choose what version of the website to display
    public function is_user_head_admin($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

        $this->db->where('email', $email); //prepare the sql statement
        $this->db->where('head_admin', '1');
        $result = $this->db->get('admins'); //pick the table to select form        
        if ($result->num_rows() == 1)
            return TRUE; //email is an admin
        else
            return FALSE;
    }

    //insert a new admin
    //returns true false
    //post data array must be named properly in order to insert the proper information
    //this function assumes check boxes are used to insert boolean values into the database
    //check function for naming conventions of the check boxes
    //the admin_email param is empty string by default ""
    //if the param is left as empty string the function will use the post array for the email
    public function insert_admin($admin_email = "") {

        if ($admin_email == "")
            $admin_email = $this->input->post('email');

        //check to see if the entry already exists
        $this->db->where('email', $admin_email); //prepare the sql statement
        $result = $this->db->get('admins'); //pick the table to select form        
        if ($result->num_rows() > 0)
            return FALSE; //psotion already exists
        else {
            //insert the user information into the user table
            //create the user account
            $insert_admin = array(
                'email' => $admin_email,
                'admin_approved' => 1,
                'head_admin' => $this->input->post('head_admin'), //determines if the admin can add other admins or not
                'monitor_election' => $this->input->post('monitor_election'),
                'add_candidate' => $this->input->post('add_candidate'),
                'create_election' => $this->input->post('create_election'),
                'remove_election' => $this->input->post('remove_election'),
                'remove_candidate' => $this->input->post('remove_candidate'),
                'approve_registration' => $this->input->post('approve_registration')
            );

            $insert = $this->db->insert('admins', $insert_admin);

            if ($this->db->affected_rows() > 0)
                return TRUE;
            else
                return FALSE;
        }
    }

    //the admin_email param is empty string by default ""
    //if the param is left as empty string the function will use the post array for the email
    public function delete_admin($admin_email = "") {
        if ($admin_email == "")
            $admin_email = $this->input->post('email');

        $this->db->where('email', $admin_email);
        $update = $this->db->delete('admins');

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //returns an array indicating what the admin is allowed to do
    //returns booleans for all values but email
    //the admin_email param is empty string by default ""
    //if the param is left as empty string the function will use the post array for the email
    public function get_admin_permissions($admin_email = "") {
        if ($admin_email == "")
            $admin_email = $this->input->post('email');

        $this->db->where('email', $admin_email); //prepare the sql statement
        $query = $this->db->get('admins'); //pick the table to select form       

        $result = array();
        foreach ($query->result() as $row) {
            $result['email'][] = $row->email;
            $result['head_admin'][] = $row->head_admin;
            $result['approved'][] = $row->admin_approved; //is the admin allowed to make changes or not
            $result['monitor_election'][] = $row->monitor_election;
            $result['add_candidate'][] = $row->add_candidate;
            $result['create_election'][] = $row->create_election;
            $result['remove_election'][] = $row->remove_election;
            $result['remove_candidate'][] = $row->remove_candidate;
            $result['approve_registration'][] = $row->approve_registration;
        }

        return $result;
    }

    //same as the get_admin_permissions except this function returns all permissions for all admins   
    public function get_all_admin_permissions() {
        $query = $this->db->get('admins'); //pick the table to select form 

        $result = array();
        foreach ($query->result() as $row) {
            $result['email'][] = $row->email;
            $result['head_admin'][] = $row->head_admin;
            $result['approved'][] = $row->admin_approved; //is the admin allowed to make changes or not
            $result['monitor_election'][] = $row->monitor_election;
            $result['add_candidate'][] = $row->add_candidate;
            $result['create_election'][] = $row->create_election;
            $result['remove_election'][] = $row->remove_election;
            $result['remove_candidate'][] = $row->remove_candidate;
            $result['approve_registration'][] = $row->approve_registration;
        }
        return $result;
    }

    //modify the admins permissions
    //uses the post array to gather the needed boolean information for the permissions
    //uses the post array to gather the email of the admin you wish to modify the permissions of
    //the admin_email param is empty string by default ""
    //if the param is left as empty string the function will use the post array for the email
    public function modify_admin_permissions($admin_email = "") {
        if ($admin_email == "")
            $admin_email = $this->input->post('email');

        //insert the user information into the user table
        //create the user account
        $modify_admin = array(
            'head_admin' => $this->input->post('head_admin'), //determines if the admin can add other admins or not
            'monitor_election' => $this->input->post('monitor_election'),
            'add_candidate' => $this->input->post('add_candidate'),
            'create_election' => $this->input->post('create_election'),
            'remove_election' => $this->input->post('remove_election'),
            'remove_candidate' => $this->input->post('remove_candidate'),
            'approve_registration' => $this->input->post('approve_registration')
        );

        $this->db->where('email', $admin_email);
        $update = $this->db->update('admins', $modify_admin);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    // select all the elections that are currently taking place based on the current time
    //RETURNS an array of elections with the indices of
    ////NOTE: index 0 will always be returned as postion: 'None' endDate: '--'
    // $result['position'], $result['endDate']
    public function get_current_elections_for_registration() {
        $sql = "SELECT position, election_over FROM elections WHERE registration_start <= NOW() AND registration_over > NOW()";
        $query = $this->db->query($sql);

        $result = array();

        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['endDate'][] = $row->election_over;
        }
        return $result;
    }

    //creates a new election and adds it to the database    
    //the admin_email param is empty string by default ""
    //if the param is left as empty string the function will use the post array for the email
    //POST INFORMATION
    // election name is a string of 50
    // description string of 250
    // eligibility string of 300
    // registration and election start / over are of type datetime
    // data times must be in the format of "yyyy-mm-dd hh:mm:ss", example: "2014-10-01 09:32:15"
    public function create_new_election($position = "") {
        if ($position == "")
            $position = $this->input->post('election_name');

        //check to see if the entry already exists
        $this->db->where('position', $position); //prepare the sql statement
        $result = $this->db->get('elections'); //pick the table to select form        
        if ($result->num_rows() > 0)
            return FALSE; //psotion already exists
        else {

            //insert the user information into the user table
            //create the user account
            $create_election = array();
            $create_election = array(
                'position' => $position,
                'description' => $this->input->post('description'),
                'eligibility' => $this->input->post('eligibility'),
                'registration_start' => $this->input->post('registration_start'),
                'registration_over' => $this->input->post('registration_over'),
                'election_start' => $this->input->post('election_start'),
                'election_over' => $this->input->post('election_over')
            );

            $insert = $this->db->insert('elections', $create_election);

            if ($this->db->affected_rows() > 0)
                return TRUE;
            else
                return FALSE;
        }
    }

    //remove the election
    //this will only work if the election is already over
    //also removes all the candidates that were running for the election
    public function remove_election($position = "") {
        if ($position == "")
            $position = $this->input->post('position');

        $this->db->where('position', $position);
        if ($this->db->delete('elections') === FALSE)
            return FALSE;

        $this->db->where('position', $position);
        if ($this->db->delete('candidate_positions') === FALSE)
            return FALSE;

        //find all the candidates that are not in the candidate postions table
        //this will return all candidates that are not running for any positions anymore
        $sql = "SELECT c.email FROM candidate c WHERE c.email NOT IN (SELECT email FROM candidate_positions)";
        $query = $this->db->query($sql);

        //the candidate is not running for anythign anymore remove them from the candidates list
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $this->db->where('email', $row->email);
                if ($this->db->delete('candidate') === FALSE)
                    return FALSE;
            }
        }
        //delete write in candidates
        $this->db->where('position', $position);
        $this->db->delete('write_in_candidate');

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //modify an election that has already been created
    //election name is unmodifiable since you will have to use it as a unique key for the database
    //the election_name param is empty string by default ""
    //if the param is left as empty string the function will use the post array for the email
    //POST INFORMATION
    // election name is a string of 50
    // description string of 250
    // eligibility string of 300
    // registration and election start / over are of type datetime
    // data times must be in the format of "yyyy-mm-dd hh:mm:ss", example: "2014-10-01 09:32:15"
    public function modify_election($election_name = "") {
        if ($election_name == "")
            $election_name = $this->input->post('election_name');

        //insert the user information into the user table
        //create the user account
        $modify_election = array(
            'description' => $this->input->post('description'),
            'eligibility' => $this->input->post('eligibility'),
            'registration_start' => $this->input->post('registration_start'),
            'registration_over' => $this->input->post('registration_over'),
            'election_start' => $this->input->post('election_start'),
            'election_over' => $this->input->post('election_over')
        );

        $this->db->where('position', $election_name);
        $update = $this->db->update('elections', $modify_election);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //returns information about the requested election
    ////RETURNS THE WINNER OF THE ELECTION
    // ARRAY INDEX RETURNS 
    // position (ie election_name) string 50
    // description  string 250
    // feedback string 500
    // eligibility string 300
    // registration and election start/ over datetime format
    // email  email of user that is winning
    // vote_number number of votes the leader has
    public function get_election_information_and_feedback_for_winner($election_name = "") {
        if ($election_name == "")
            $election_name = $this->input->post('position');

        $candidate_info = "SELECT vote_number, email FROM candidate_positions WHERE position = ? ORDER BY vote_number DESC LIMIT 1";
        $candidate_query = $this->db->query($candidate_info, array($election_name));

        $election_info = "SELECT e.* FROM elections e WHERE position = ?";
        $election_query = $this->db->query($election_info, array($election_name));

        $result = array();
        if ($election_query->num_rows() > 0) {
            $result['position'] = $election_query->result()[0]->position;
            $result['description'] = $election_query->result()[0]->description;
            $result['feedback'] = $election_query->result()[0]->feedback;
            $result['eligibility'] = $election_query->result()[0]->eligibility;
            $result['registration_start'] = $election_query->result()[0]->registration_start;
            $result['registration_over'] = $election_query->result()[0]->registration_over;
            $result['election_start'] = $election_query->result()[0]->election_start;
            $result['election_over'] = $election_query->result()[0]->election_over;
        } else {
            $result['position'] = "";
            $result['description'] = "";
            $result['feedback'] = "";
            $result['eligibility'] = "";
            $result['registration_start'] = "";
            $result['registration_over'] = "";
            $result['election_start'] = "";
            $result['election_over'] = "";
        }

        if ($candidate_query->num_rows() > 0) {
            $result['email'] = $candidate_query->result()[0]->email;
            $result['vote_number'] = $candidate_query->result()[0]->vote_number;
        } else {
            $result['email'] = "";
            $result['vote_number'] = "";
        }
        return $result;
    }

    //returns the results for the requested election
    //returns all votes for the election
    // ARRAY INDEX RETURNS 
    // position (ie election_name) string 50
    // description  string 250
    // feedback string 500
    // eligibility string 300
    // registration and election start/ over datetime format
    // NOTE: the email and vote_number are listed in a descending associative array, so index 0 is the winner
    // email  email of user that is winning
    // vote_number number of votes the leader has
    public function get_election_all_votes($position = "") {
        if ($position == "")
            $position = $this->input->post('position');

        $candidate_info = "SELECT vote_number, email FROM candidate_positions WHERE position = ? ORDER BY vote_number DESC";
        $candidate_query = $this->db->query($candidate_info, array($position));

        $election_info = "SELECT e.* FROM elections e WHERE position = ?";
        $election_query = $this->db->query($election_info, array($position));

        $result = array();
        $result['position'] = $election_query->result()[0]->position;
        $result['description'] = $election_query->result()[0]->description;
        $result['feedback'] = $election_query->result()[0]->feedback;
        $result['eligibility'] = $election_query->result()[0]->eligibility;
        $result['registration_start'] = $election_query->result()[0]->registration_start;
        $result['registration_over'] = $election_query->result()[0]->registration_over;
        $result['election_start'] = $election_query->result()[0]->election_start;
        $result['election_over'] = $election_query->result()[0]->election_over;

        foreach ($candidate_query->result() as $row) {
            $result['email'][] = $row->email;
            $result['vote_number'][] = $row->vote_number;
        }

        //get all write in candidates and their vote count
        $sql = "SELECT w.email, w.vote_count
            FROM write_in_candidate w
            WHERE w.position = ?";
        $query = $this->db->query($sql, array($position));

        foreach ($query->result() as $row) {
            $result['writein_email'][] = $row->email;
            $result['writein_count'][] = $row->vote_count;
        }

        return $result;
    }

    //returns information like vote count for on going elections
    //returns just general info about the election like the candidate names
    //also returns the total number of votes across all candidates
    public function view_election_votes($position = "") {
        if ($position == "")
            $position = $this->input->post('position');


        $sql = "SELECT u.first_name, u.last_name, cp.vote_number, cp.position 
            FROM users u, candidate_positions cp, elections e 
            WHERE  e.position = ? AND e.position = cp.position AND cp.email = u.email";
        $query = $this->db->query($sql, array($position));

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['first_name'][] = $row->first_name;
            $result['last_name'][] = $row->last_name;
            $total_votes += (int) $row->vote_number;
            $result['vote_number'][] = $row->vote_number;
            $result['position'][] = $row->position;
        }
        //get all write in candidates and their vote count
        $sql = "SELECT w.email, w.vote_count
            FROM write_in_candidate w
            WHERE w.position = ?";
        $query = $this->db->query($sql, array($position));

        foreach ($query->result() as $row) {
            $result['writein_email'][] = $row->email;
            $result['writein_count'][] = $row->vote_count;
            $total_votes += (int) $row->vote_count;
        }
        $result['total_votes'] = $total_votes;

        return $result;
    }

    //returns the information for elections that have been requested to be re-ran
    public function get_re_election_requests() {
        $this->db->where('reelection_requested', 1);
        $query = $this->db->get('elections'); //pick the table to select form 

        $result = array();
        foreach ($query->result() as $row) {
            $result['position'] = $row->position;
            $result['description'] = $row->description;
            $result['feedback'] = $row->feedback;
            $result['eligibility'] = $row->eligibility;
            $result['registration_start'] = $row->registration_start;
            $result['registration_over'] = $row->registration_over;
            $result['election_start'] = $row->election_start;
            $result['election_over'] = $row->election_over;
        }

        return $result;
    }

    //declare the election results as valid and final
    public function finalize_election_results($position = "") {
        if ($position == "")
            $position = $this->input->post('position');

        $final = array(
            'final_result' => 1
        );


        $this->db->where('position', $position);
        $update = $this->db->update('elections', $final);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //insert feedback for the given election into the database
    //the election_name param is empty string by default ""
    //if the param is left as empty string the function will use the post array for the email
    //looks for a string that was stored in the post array called 'feedback'
    public function give_feedback($election_name = "") {
        if ($election_name == "")
            $election_name = $this->input->post('election_name');

        $modify_feedback = array(
            'feedback' => $this->input->post('feedback')
        );

        $this->db->where('position', $election_name);
        $update = $this->db->update('elections', $modify_feedback);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //returns all the information for all approved voters
    //multiple users will show up if they are approved for multiple elections
    public function get_all_approved_voters() {
        $sql = "SELECT u.*, v.position FROM users u, voter v WHERE v.email = u.email AND v.approved = 1";
        $approved_query = $this->db->query($sql);

        $result = array();
        foreach ($approved_query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; //is the admin allowed to make changes or not
            $result['last_name'][] = $row->last_name;
            $result['untID'][] = $row->untID;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['department'][] = $row->department;
            $result['major'][] = $row->major;
            $result['year'][] = $row->year;
            $result['position'][] = $row->position;
        }
        return $result;
    }

    //returns all the information for all approved candidates
    //multiple users will show up if they are approved for multiple elections
    public function get_all_approved_candidates() {

        $sql = "SELECT u.*, c.bio, c.held_positions, cp.position 
            FROM users u, candidate c, candidate_positions cp 
            WHERE cp.approved = 1 AND u.email = c.email AND c.email = cp.email";
        $approved_query = $this->db->query($sql);

        $result = array();
        foreach ($approved_query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; //is the admin allowed to make changes or not
            $result['last_name'][] = $row->last_name;
            $result['untID'][] = $row->untID;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['department'][] = $row->department;
            $result['major'][] = $row->major;
            $result['year'] = $row->year;
            $result['position'][] = $row->position;
            $result['bio'][] = $row->bio;
            $result['held_positions'][] = $row->held_positions;
        }
        return $result;
    }

    //returns all the information for all voter requests
    //multiple users will show up if they are approved for multiple elections
    public function get_all_voter_requests() {

        $sql = "SELECT u.*, v.position FROM users u, voter v WHERE v.email = u.email AND v.approved = 0";
        $approved_query = $this->db->query($sql);

        $result = array();
        foreach ($approved_query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; //is the admin allowed to make changes or not
            $result['last_name'][] = $row->last_name;
            $result['untID'][] = $row->untID;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['department'][] = $row->department;
            $result['major'][] = $row->major;
            $result['year'][] = $row->year;
            $result['position'][] = $row->position;
        }
        return $result;
    }

    //returns all the information for all unapproved candidates
    //multiple users will show up if they are approved for multiple elections
    public function get_all_candidate_requests() {

        $sql = "SELECT u.*, c.bio, c.held_positions, cp.position 
             FROM users u, candidate c, candidate_positions cp 
            WHERE cp.approved = 0 AND u.email = c.email AND c.email = cp.email";
        $approved_query = $this->db->query($sql);

        $result = array();
        foreach ($approved_query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; //is the admin allowed to make changes or not
            $result['last_name'][] = $row->last_name;
            $result['untID'][] = $row->untID;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['department'][] = $row->department;
            $result['major'][] = $row->major;
            $result['year'][] = $row->year;
            $result['position'][] = $row->position;
            $result['bio'][] = $row->bio;
            $result['held_positions'][] = $row->held_positions;
        }
        return $result;
    }

    //returns all the user information and all the elections they are approved to vote in
    public function get_all_approved_elections_for_voter($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

        $sql = "SELECT u.*, v.position FROM users u, voter v WHERE v.email = u.email AND v.approved = 1 AND v.email = ?";
        $approved_query = $this->db->query($sql, array($email));

        $result = array();

        $result['email'] = $approved_query->result()[0]->email;
        $result['first_name'] = $approved_query->result()[0]->first_name; //is the admin allowed to make changes or not
        $result['last_name'] = $approved_query->result()[0]->last_name;
        $result['untID'] = $approved_query->result()[0]->untID;
        $result['faculty_student'] = $approved_query->result()[0]->faculty_student;
        $result['college'] = $approved_query->result()[0]->college;
        $result['department'] = $approved_query->result()[0]->department;
        $result['major'] = $approved_query->result()[0]->major;
        $result['year'] = $approved_query->result()[0]->year;
        foreach ($approved_query->result() as $row) {
            $result['position'][] = $row->position;
        }
        return $result;
    }

    //approve a candidate to run for a specific position
    public function approve_candidate($email = "", $position = "") {
        if ($email == "")
            $email = $this->input->post('email');
        if ($position == "")
            $position = $this->input->post('position');

        $approve = array(
            'approved' => 1
        );

        $this->db->where('email', $email);
        $this->db->where('position', $position);
        $update = $this->db->update('candidate_positions', $approve);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //approve a vote to vote for a specific position
    public function approve_voter($email = "", $position = "") {
        if ($email == "")
            $email = $this->input->post('email');
        if ($position == "")
            $position = $this->input->post('position');

        $approve = array(
            'approved' => 1
        );

        $this->db->where('email', $email);
        $this->db->where('position', $position);
        $update = $this->db->update('voter', $approve);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //deletes the candidate from the candidate tables since he was disapproved
    public function disapprove_candidate($email = "", $position = "") {
        if ($email == "")
            $email = $this->input->post('email');
        if ($position == "")
            $position = $this->input->post('position');

        $del = FALSE;
        $sql = "DELETE FROM candidate_positions WHERE email = ? AND position = ?";
        $del = $this->db->query($sql, array($email, $position));

        $sql = "SELECT c.email FROM candidate c WHERE c.email = ? AND c.email IN (SELECT email FROM candidate_positions)";
        $query = $this->db->query($sql, array($email));
        //if any rows are returned than that means the email as positions left and dont delete him from the candidate table
        if ($query->num_rows() === 0) {
            $sql = "DELETE FROM candidate WHERE email = ?";
            $del = $this->db->query($sql, array($email));
        }

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //deletes the voter from the voter tables since he was disapproved
    public function disapprove_voter($email = "", $position = "") {
        if ($email == "")
            $email = $this->input->post('email');
        if ($position == "")
            $position = $this->input->post('position');

        $sql = "DELETE FROM voter WHERE email = ? AND position = ?";
        $this->db->query($sql, array($email, $position));

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    public function get_all_users() {
        $query = $this->db->get('users');

        $result = array();
        foreach ($query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; //is the admin allowed to make changes or not
            $result['last_name'][] = $row->last_name;
            $result['untID'][] = $row->untID;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['department'][] = $row->department;
            $result['major'][] = $row->major;
            $result['year'][] = $row->year;
        }
        return $result;
    }

    //returns all the candidates approved or not
    public function get_all_candidates() {
        $sql = "SELECT u.*, c.bio, c.held_positions, cp.position 
            FROM users u,candidate c,candidate_positions cp 
            WHERE u.email = c.email AND c.email = cp.email";
        $approved_query = $this->db->query($sql);

        $result = array();
        foreach ($approved_query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; //is the admin allowed to make changes or not
            $result['last_name'][] = $row->last_name;
            $result['untID'][] = $row->untID;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['department'][] = $row->department;
            $result['major'][] = $row->major;
            $result['year'][] = $row->year;
            $result['position'][] = $row->position;
            $result['bio'][] = $row->bio;
            $result['held_positions'][] = $row->held_positions;
        }
        return $result;
    }

    //returns all the votres approved or not
    public function get_all_voters() {
        $sql = "SELECT u.*, v.position FROM users u, voter v WHERE v.email = u.email";
        $approved_query = $this->db->query($sql);

        $result = array();
        foreach ($approved_query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; //is the admin allowed to make changes or not
            $result['last_name'][] = $row->last_name;
            $result['untID'][] = $row->untID;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['department'][] = $row->department;
            $result['major'][] = $row->major;
            $result['year'][] = $row->year;
            $result['position'][] = $row->position;
        }
        return $result;
    }

    //custom validation callback for the admins email
    //checks to see if the email already exists for an admin
    public function check_if_admin_email_exists($requested_email) {
        $this->db->where('email', $requested_email); //prepare the sql statement
        $result = $this->db->get('admins'); //pick the table to select form        
        if ($result->num_rows() > 0)
            return FALSE; //email already registered
        else
            return TRUE;
    }
    
    //returns information about users that have not voted for the specified election yet
    //returns: email, first_name, last_name, and position
    public function get_all_voters_that_havent_voted($position = ""){
       if ($position == "")
            $position = $this->input->post('position');
            
            $sql = "SELECT u.*, v.position
                    FROM voter v, users u
                    WHERE v.position = ? AND v.email = u.email AND v.voted ='0' AND v.approved = '1'";
            
            $query = $this->db->query($sql,array($position));                     
            
                  $result = array();
        foreach ($query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; 
            $result['last_name'][] = $row->last_name;           
            $result['position'][] = $row->position;
        }
        return $result;
    }

//RETURNS all the user information for the given id in an array
//returns false if the id is not found
    public function get_receipt_info_by_id($id = ""){
    if ($id == "")
            $id = $this->input->post('id');
            
            $sql = "SELECT v.id, v.email, v.position, v.vote_date, v.voted_for,
            u.first_name, u.last_name, u.untID, u.faculty_student, u.college,u.department,u.major,u.year
            FROM voter v, users u
            WHERE v.email = u.email AND v.id = ?";
            
             $query = $this->db->query($sql,array($id));
             
             $result = array();
                 
             if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    $result['id'] = $row->id;
                    $result['email'] = $row->email; 
                    $result['position'] = $row->position;           
                    $result['vote_date'] = $row->vote_date;
                    $result['first_name'] = $row->first_name;
                    $result['last_name'] = $row->last_name;
                    $result['untID'] = $row->untID;
                    $result['faculty_student'] = $row->faculty_student;
                    $result['college'] = $row->college;
                    $result['department'] = $row->department;
                    $result['major'] = $row->major;
                    $result['year'] = $row->year;     
                }
                return $result;             
            }
            else 
                return FALSE;           
    }
}

?>
