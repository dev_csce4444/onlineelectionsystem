<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_model extends CI_Model {

    public function sendMail($email = "", $subject = "", $mess = "") {

        if ($email == "")
            $email = $this->input->post('email');

        if ($mess == "")
            $mess = "This message has been sent from UNT online elections.";

        if ($subject == "")
            $subject = 'UNT Online Elections';

        $header = "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $header .= 'From: UNT Online Elections <electionmonitor4444@gmail.com>' . "\r\n";

        if (mail(trim($email), $subject, $mess, $header)) {     
            return TRUE;
        } else {     
            return FALSE;
        }
    }

}

?>