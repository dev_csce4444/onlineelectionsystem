<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Candidate_model extends CI_Model {

    //returns information about the candidate, ignores if they are approved or not
    //use this to populat ethe editing candidate info view
    //will return an associative array of all the positions they are running for if they are approved or not and what their current vote count is
    public function get_candidate_info($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

        $sql = "SELECT u.*, c.bio, c.held_positions, cp.position, cp.approved, cp.vote_number 
            FROM users u NATURAL JOIN candidate c NATURAL JOIN candidate_positions cp 
            WHERE u.email = ? AND c.email = ? AND cp.email = ?";
        $approved_query = $this->db->query($sql, array($email, $email, $email));

        $result = array();
        $result['email'] = $approved_query->result()[0]->email;
        $result['first_name'] = $approved_query->result()[0]->first_name; //is the admin allowed to make changes or not
        $result['last_name'] = $approved_query->result()[0]->last_name;
        $result['untID'] = $approved_query->result()[0]->untID;
        $result['faculty_student'] = $approved_query->result()[0]->faculty_student;
        $result['college'] = $approved_query->result()[0]->college;
        $result['department'] = $approved_query->result()[0]->department;
        $result['major'] = $approved_query->result()[0]->major;
        $result['year'] = $approved_query->result()[0]->year;
        $result['bio'] = $approved_query->result()[0]->bio;
        $result['held_positions'] = $approved_query->result()[0]->held_positions;

        foreach ($approved_query->result() as $row) {
            $result['position'][] = $row->position;
            $result['approved'][] = $row->approved;
            $result['vote_number'][] = $row->vote_number;
        }
        return $result;
    }

    // check to see if the specific email is a candidate email account or not
    // use to choose what version of the website to display
    public function is_user_a_candidate($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

        $this->db->where('email', $email); //prepare the sql statement
        $result = $this->db->get('candidate'); //pick the table to select form        
        if ($result->num_rows() == 1)
            return TRUE; //email is a candidate
        else
            return FALSE;
    }

    //use this the show other users info about the candidate
    //returns general info about the candidate
    public function view_candidate_info($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

        $sql = "SELECT u.*, c.bio, c.held_positions 
            FROM users u NATURAL JOIN candidate c 
            WHERE u.email = ? AND c.email = ?";
        $approved_query = $this->db->query($sql, array($email, $email));

        $result = array();
        $result['first_name'] = $approved_query->result()[0]->first_name; //is the admin allowed to make changes or not
        $result['last_name'] = $approved_query->result()[0]->last_name;
        $result['faculty_student'] = $approved_query->result()[0]->faculty_student;
        $result['college'] = $approved_query->result()[0]->college;
        $result['department'] = $approved_query->result()[0]->department;
        $result['major'] = $approved_query->result()[0]->major;
        $result['year'] = $approved_query->result()[0]->year;
        $result['bio'] = $approved_query->result()[0]->bio;
        $result['held_positions'] = $approved_query->result()[0]->held_positions;

        return $result;
    }

    //update the candidates bio and held positions
    //data info
    // bio string of 500 
    // held positions string of 250
    public function update_bio_held_positions($email = "",$bio = "") {
        if ($email == "")
            $email = $this->input->post('email');

        if ($bio =="")
        $bio = array(
            'bio' => $this->input->post('bio'),
            'held_positions' => $this->input->post('held_positions')
        );

        $this->db->where('email', $email);
        $update = $this->db->update('candidate', $bio);

             if($this->db->affected_rows() > 0) return TRUE;
        else return FALSE; 
    }

    //returns all the information for all candidates
    //multiple users will show up if they are approved for multiple elections
    //does not return the position they are running for
    public function get_all_candidates() {

        $sql = "SELECT u.*, c.bio, c.held_positions FROM users u NATURAL JOIN candidate c ";
        $approved_query = $this->db->query($sql);

        $result = array();
        foreach ($approved_query->result() as $row) {
            $result['email'][] = $row->email;
            $result['first_name'][] = $row->first_name; //is the admin allowed to make changes or not
            $result['last_name'][] = $row->last_name;
            $result['untID'][] = $row->untID;
            $result['faculty_student'][] = $row->faculty_student;
            $result['college'][] = $row->college;
            $result['department'][] = $row->department;
            $result['major'][] = $row->major;
            $result['year'] = $row->year;
            $result['bio'][] = $row->bio;
            $result['held_positions'][] = $row->held_positions;
        }
        return $result;
    }

}

?>
