<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Membership_model extends CI_Model {

    //login will use the post array super global to locate the entered email and password and check to see if it is a valid user
    // returns true or false
    public function login() {

        $this->db->where('email', $this->input->post('email'));
        $this->db->where('password', md5($this->input->post('password')));

        $query = $this->db->get('users');

        if ($query->num_rows == 1) {
            $data = array(
                'email' => $this->input->post('email'),
                'is_logged_in' => true
            );

            $this->session->set_userdata($data);
            return TRUE;
        }
        else
            return FALSE;
    }

    //log the user out
    //set the session data to be flagged as loged out
    //destroys the session
    public function logout($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

        $data = array(
            'email' => $this->input->post('email'),
            'is_logged_in' => false
        );

        $this->session->set_userdata($data);
        $this->session->sess_destroy();
        return TRUE;
    }

    //allow the user to apply to be a voter for a given election
    //this will insert the user into the voter table untill they are approved or not
    //if the user already is a voter for the position it returns false
    public function apply_to_be_voter($email = "", $position = "") {
        if ($email == "")
            $email = $this->input->post('email');

        if ($position == "")
            $position = $this->input->post('position');

        $this->db->where('email', $email); //prepare the sql statement
        $this->db->where('position', $position); //prepare the sql statement
        $result = $this->db->get('voter'); //pick the table to select form        
        if ($result->num_rows() > 0)
            return FALSE; //




            
//add the e-mail position in the voter table
        else {

            $insert_voter = array(
                'email' => $email,
                'position' => $position
            );
            $insert = $this->db->insert('voter', $insert_voter);
            return $insert;
        }
    }

    //allow the user to apply to be a candidate for a given election
    //this will insert the user into the candidate table untill they are approved or not
    //if the user already is a candidate for the position it returns false
    public function apply_to_be_candidate($email = "", $position = "") {
        if ($email == "")
            $email = $this->input->post('email');

        if ($position == "")
            $position = $this->input->post('position');

        $this->db->where('email', $email); //prepare the sql statement
        $result = $this->db->get('candidate');

        //if the user isnt in the candidate table add them
        $insert = TRUE;
        if ($result->num_rows() == 0) {
            $insert_candidate = array(
                'email' => $email
            );
            $insert = $this->db->insert('candidate', $insert_candidate);
        }

        if ($insert) {
            $this->db->where('email', $email);
            $this->db->where('position', $position);
            $result = $this->db->get('candidate_positions');

            if ($result->num_rows() > 0)
                return FALSE;
//add the e-mail position in the candidate table
            else {

                $insert_candidate = array(
                    'email' => $email,
                    'position' => $position
                );
                $insert = $this->db->insert('candidate_positions', $insert_candidate);
                return $insert;
            }
        }
        else
            return $insert; //return false
    }

    //get the email entered from the post array to use as the username
    //attemps to create the account
    //returns true false
    public function insertUser() {

        //get the data from the elections table to compare to the post date
        //this will make sure the indices are aligned with what the post array will return
        $sql = "SELECT position, election_over FROM elections WHERE registration_start <= NOW() AND registration_over > NOW()";
        $query = $this->db->query($sql);

        $result = array();
        $result['position'][] = 'None';
        $result['endDate'][] = '--';

        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['endDate'][] = $row->election_over;
        }

//insert the user into the voter table for all elections they have indicated
        $voters = array();
        $voters[0] = 0; //set index to 0 incase the voter post wasnt set
        $voters = $this->input->post('applyVoter');
        for ($i = 0; $i < count($voters); $i++) {
            if ($voters[$i] == 0)
                break; //the non option was selected to break out of the loop and insert nothing
            $new_voter_insert_data = array(
                'email' => $this->input->post('email'),
                'position' => $result['position'][$voters[$i]], //grab the position name from the elections table that has the same index as the index passed in via post 
                'voted' => 0, //has the user already voted for this election
                'approved' => 0
            );

            $insert_voter = $this->db->insert('voter', $new_voter_insert_data);
            //if an error is returned return false
            if ($insert_voter === FALSE)
                return $insert_voter;
        }


        //add the user to the candidates table
        $candidate = array();
        $candidate[0] = 0; //set index to 0 incase the candidate post wasnt set
        $candidate = $this->input->post('applyCandidate');
        //if none was selected then dont insert anything
        if ($candidate[0] != 0) {

            $new_candidate = array(
                'email' => $this->input->post('email'),
                'bio' => "",
                'held_positions' => "" //has the user already voted for this election                
            );

            if ($this->db->insert('candidate', $new_candidate) === FALSE)
                return FALSE;

            //insert all the positions the cadidate wishes to run for into the candidate_positions table
            for ($i = 0; $i < count($candidate); $i++) {
                $new_candidate_insert_data = array(
                    'email' => $this->input->post('email'),
                    'position' => $result['position'][$candidate[$i]], //grab the position name from the elections table that has the same index as the index passed in via post 
                    'approved' => 0,
                    'vote_number' => 0
                );

                if ($this->db->insert('candidate_positions', $new_candidate_insert_data) === FALSE)
                    return FALSE;
            }
        }

        //insert the user information into the user table
        //create the user account
        $new_member_insert_data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),
            'untid' => $this->input->post('UNTid'),
            'faculty_student' => $this->input->post('faculty_student'),
            'college' => $this->input->post('college'),
            'department' => $this->input->post('department'),
            'major' => $this->input->post('major'),
            'year' => $this->input->post('year')
        );

        $insert = $this->db->insert('users', $new_member_insert_data);

        return $insert;
    }

    //custom validation callback for the users email
    public function check_if_email_exists($requested_email) {
        $this->db->where('email', $requested_email); //prepare the sql statement
        $result = $this->db->get('users'); //pick the table to select form        
        if ($result->num_rows() > 0)
            return FALSE; //email already registered
        else
            return TRUE;
    }

    //check to make sure the id has not been entered already
    public function check_unique_id($request_id) {
        $this->db->where('untID', $request_id); //prepare the sql statement
        $result = $this->db->get('users'); //pick the table to select form        
        if ($result->num_rows() > 0)
            return FALSE; //email already registered
        else
            return TRUE;
    }

    // select all the elections that are currently taking place based on the current time
    //RETURNS an array of elections with the indices of
    ////NOTE: index 0 will always be returned as postion: 'None' endDate: '--'
    // $result['position'], $result['endDate']
    public function get_current_elections_for_registration() {
        $sql = "SELECT position, election_over FROM elections WHERE registration_start <= NOW() AND registration_over > NOW()";
        $query = $this->db->query($sql);

        $result = array();
        $result['position'][] = 'None';
        $result['endDate'][] = '--';

        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['endDate'][] = $row->election_over;
        }
        return $result;
    }

    //returns the email and password so you can email it to the user
    public function password_recovery($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

        $sql = "SELECT email, password FROM users WHERE email = ?";
        $query = $this->db->query($sql, array($email));

        $result = array();
        if ($query->num_rows() > 0) {
            $result['email'] = $query->result()[0]->email;
            $result['password'] = md5($query->result()[0]->password);
        }

        return $result;
    }
    
    //change users password, expects it to not be encypted
    public function change_password($email = "", $oldPassword ="",$newPassword=""){
        if ($email == "")
            $email = $this->input->post('email');
        if ($oldPassword == "")
            $oldPassword = $this->input->post('oldPassword');
        if ($newPassword == "")
            $newPassword = $this->input->post('newPassword');
    
        $sql = "SELECT * FROM users WHERE email = ? AND password = ?";
        $query = $this->db->query($sql,array($email,md5($oldPassword)));
         if ($query->num_rows() > 0) {
          
                   $pass = array(
            'password' => md5($newPassword)
        );

        
        $this->db->where('email', $email);
        $update = $this->db->update('users',$pass);        
        return TRUE;
         }
         else return FALSE;
        
        
        
        
     
        
    }

}

?>