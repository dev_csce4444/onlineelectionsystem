<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Election_model extends CI_Model {

//returns all elections that have finished and the admin have FINALIZED the results
//returns an array of elections
    public function get_all_finished_finalized_elections() {

        $sql = "SELECT e.* 
            FROM elections e 
            WHERE  e.election_over <= NOW() AND final_result = '1'";
        $query = $this->db->query($sql);

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['description'][] = $row->description;
            $result['feedback'][] = $row->feedback;
            $result['eligibility'][] = $row->eligibility;
            $result['registration_start'][] = $row->registration_start;
            $result['registration_over'][] = $row->registration_over;
            $result['election_start'][] = $row->election_start;
            $result['election_over'][] = $row->election_over;
            $result['reelection_requested'][] = $row->reelection_requested;
        }

        return $result;
    }

    //returns all elections that have finished and the admin have NOT BEEN FINALIZED the results
    //returns an array of elections
    public function get_all_finished_elections() {

        $sql = "SELECT e.* 
            FROM elections e 
            WHERE  e.election_over <= NOW()";
        $query = $this->db->query($sql);

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['description'][] = $row->description;
            $result['feedback'][] = $row->feedback;
            $result['eligibility'][] = $row->eligibility;
            $result['registration_start'][] = $row->registration_start;
            $result['registration_over'][] = $row->registration_over;
            $result['election_start'][] = $row->election_start;
            $result['election_over'][] = $row->election_over;
            $result['reelection_requested'][] = $row->reelection_requested;
        }

        return $result;
    }

    //returns an array of all the elections that have not started yet
    public function get_all_up_coming_elections() {

        $sql = "SELECT e.* 
            FROM elections e 
            WHERE  e.election_start >= NOW()";
        $query = $this->db->query($sql);

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['description'][] = $row->description;
            $result['feedback'][] = $row->feedback;
            $result['eligibility'][] = $row->eligibility;
            $result['registration_start'][] = $row->registration_start;
            $result['registration_over'][] = $row->registration_over;
            $result['election_start'][] = $row->election_start;
            $result['election_over'][] = $row->election_over;
            $result['reelection_requested'][] = $row->reelection_requested;
        }

        return $result;
    }

    //returns all the elections that one can register for
    public function get_all_current_elections() {

        $sql = "SELECT e.* 
            FROM elections e 
            WHERE  e.election_over > NOW() AND e.election_start <= NOW()";
        $query = $this->db->query($sql);

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['description'][] = $row->description;
            $result['feedback'][] = $row->feedback;
            $result['eligibility'][] = $row->eligibility;
            $result['registration_start'][] = $row->registration_start;
            $result['registration_over'][] = $row->registration_over;
            $result['election_start'][] = $row->election_start;
            $result['election_over'][] = $row->election_over;
            $result['reelection_requested'][] = $row->reelection_requested;
        }

        return $result;
    }

    //returns all the elections that one can register for
    public function get_all_elections_for_registration() {

        $sql = "SELECT e.* 
            FROM elections e 
            WHERE  e.registration_over > NOW() AND e.registration_start <= NOW()";
        $query = $this->db->query($sql);

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['description'][] = $row->description;
            $result['feedback'][] = $row->feedback;
            $result['eligibility'][] = $row->eligibility;
            $result['registration_start'][] = $row->registration_start;
            $result['registration_over'][] = $row->registration_over;
            $result['election_start'][] = $row->election_start;
            $result['election_over'][] = $row->election_over;
            $result['reelection_requested'][] = $row->reelection_requested;
        }

        return $result;
    }

    //returns an array of all elections, present and future
    public function get_all_elections() {
        $sql = "SELECT * 
            FROM elections";
        $query = $this->db->query($sql);

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['description'][] = $row->description;
            $result['feedback'][] = $row->feedback;
            $result['eligibility'][] = $row->eligibility;
            $result['registration_start'][] = $row->registration_start;
            $result['registration_over'][] = $row->registration_over;
            $result['election_start'][] = $row->election_start;
            $result['election_over'][] = $row->election_over;
            $result['reelection_requested'][] = $row->reelection_requested;
        }

        return $result;
    }

    //check to see if the user has voted or not for the given position
    public function has_user_voted($email = "", $position = "") {
        if ($email == "")
            $email = $this->input->post('email');
        if ($position == "")
            $position = $this->input->post('position');

        $sql = "SELECT email
            FROM voter
            WHERE email = ? AND position = ? AND voted = 0";
        $query = $this->db->query($sql, array($email, $position));
        if ($query->num_rows() > 0)
            return FALSE;
        else
            return TRUE;
    }

    // select all the elections that are currently taking place based on the current time
    //RETURNS an array of elections for which the voter is approved to vote in with the indices of
    ////NOTE: index 0 will always be returned as postion: 'None' endDate: '--'
    // $result['position'][], $result['endDate'][]
    public function get_current_eligible_elections($email = "") {
        if ($email == "")
            $email = $this->input->post('email');

      $sql = "SELECT e.position, e.election_over 
          FROM elections e, voter v 
          WHERE e.position = v.position AND e.election_start <= NOW() AND e.election_over > NOW() AND v.email = ? AND v.approved = 1 AND v.voted = 0";

        $query = $this->db->query($sql, array($email));

        $result = array();
        foreach ($query->result() as $row) {
            $result['position'][] = $row->position;
            $result['endDate'][] = $row->election_over;
        }
        return $result;
    }

    //returns what candidates are eligible for a specific election
    //returns additional information also like the email for each candidate and the position
    //can be used to populate the voting ballet
    public function get_candidates_for_election($position = "") {
        if ($position == "")
            $position = $this->input->post('position');

        $sql = "SELECT u.first_name, u.last_name, u.email, cp.position FROM users u, candidate_positions cp WHERE u.email = cp.email AND cp.position = ? AND cp.approved = 1";
        $query = $this->db->query($sql, array($position));

        $result = array();
        foreach ($query->result() as $row) {
            $result['first_name'][] = $row->first_name;
            $result['last_name'][] = $row->last_name;
            $result['email'][] = $row->email;
            $result['position'][] = $row->position;
        }
        return $result;
    }

    //RETURNS the voter id,email, and position in an array with index 0 being TRUE on success, returns only FALSE on failure no array
    //increments the vote counter for a given candidate
    //you need to pass in the candidate voted for email, the position voted for, and the voters email
    //the candidate vote count will be increased for the given position
    //the voter will be marked as already have voted for the specific election
    public function vote_for_candidate($voterEmail = "", $position = "", $candidateEmail = "") {
        if ($candidateEmail == "")
            $candidateEmail = $this->input->post('candidateEmail');
        if ($voterEmail == "")
            $voterEmail = $this->input->post('voterEmail');
        if ($position == "")
            $position = $this->input->post('position');

        $this->db->where('email', $candidateEmail);
        $this->db->where('position', $position);
        $this->db->set('vote_number', 'vote_number+1', FALSE);
        $update = FALSE;
        if ($this->db->update('candidate_positions')) {
            $this->db->where('email', $voterEmail);
            $this->db->where('position', $position);
            $update = $this->db->set('voted', '1');
            $this->db->set('vote_date', 'NOW()', FALSE);
            $this->db->set('voted_for',$candidateEmail);
            $this->db->update('voter');
        }
        if ($this->db->affected_rows() > 0){
                 $sql = "SELECT id, email, position
          FROM voter
          WHERE position = ? AND email = ?";

        $query = $this->db->query($sql, array($position,$voterEmail));
        
        $result = array();
          foreach ($query->result() as $row) {
            $result[0] = TRUE;
            $result['id'] = $row->email;
            $result['email'] = $row->email;
            $result['position'] = $row->position;
            }
            return $result;
            
            }
        else
            return FALSE;
    }

    //allow the user to write in a candidate
    //this will add the email to the write in candidate table
    //leaving the candidate email blank will default to using the post array for the email
    public function write_in_candidate($voterEmail = "", $position = "", $candidateEmail = "") {
        if ($candidateEmail == "")
            $candidateEmail = $this->input->post('candidateEmail');
        if ($voterEmail == "")
            $voterEmail = $this->input->post('voterEmail');
        if ($position == "")
            $position = $this->input->post('position');

        $update = FALSE;
        $sql = "SELECT email,position
            FROM write_in_candidate w
            WHERE w.email = ? AND w.position = ?";
        $query = $this->db->query($sql, array($candidateEmail, $position));
        if ($query->num_rows() > 0) {
            $this->db->where('email', $candidateEmail);
            $this->db->where('position', $position);
            $this->db->set('vote_count', 'vote_count+1', FALSE);
            $update = $this->db->update('write_in_candidate');
        } else {
            $writein = array(
                'email' => $candidateEmail,
                'position' => $position
            );
            $update = $this->db->insert('write_in_candidate', $writein);
        }

        if ($update) {
//mark the voter as has voted
            $this->db->where('email', $voterEmail);
            $this->db->where('position', $position);
            $update = $this->db->set('voted', '1');
            $this->db->set('vote_date', 'NOW()', FALSE);
            $this->db->set('voted_for',$candidateEmail);
            $update = $this->db->update('voter');
        }
        if ($this->db->affected_rows() > 0)
            {
                 $sql = "SELECT id, email, position
          FROM voter
          WHERE position = ? AND email = ?";

        $query = $this->db->query($sql, array($position,$email));
        
        $result = array();
          foreach ($query->result() as $row) {
            $result[0] = TRUE;
            $result['id'] = $row->email;
            $result['email'] = $row->email;
            $result['position'] = $row->position;
            }
            return $result;
            
            }
        else
            return FALSE;
    }

//returns the voting receipt id
//returns id of the voter for the position, email, position
    public function get_voter_id($email = "", $position = ""){
       if ($email == "")
            $email = $this->input->post('email');
        if ($position == "")
            $position = $this->input->post('position');
            
              $sql = "SELECT id, email, position
          FROM voter
          WHERE position = ? AND email = ?";

        $query = $this->db->query($sql, array($position,$email));
        
        $result = array();
          foreach ($query->result() as $row) {
            $result['id'] = $row->id;
            $result['email'] = $row->email;
            $result['position'] = $row->position;
            }
            return $result;
    }

//update the date in the datebase for when the last reminder was sent
//used when you click on the send reminders button
public function set_reminder_sent_date($position = ""){
 if ($position == "")
            $position = $this->input->post('position');

            $this->db->where('position', $position);
            $this->db->set('reminder_sent_date', 'NOW()', FALSE);       
            $this->db->update('elections');
            
              if ($this->db->affected_rows() > 0) 
                  return TRUE;
              else 
                  return FALSE;                                    
}

//returns the date that was stored the last time you called set_reminder_sent_date
//RETURNS the date and time if a previous email was sent, if no reminder email was sent this function returns false
//used for reminder emails
public function get_reminder_sent_date($position = ""){
   if ($position == "")
            $position = $this->input->post('position');
            
                      $sql = "SELECT position, reminder_sent_date
          FROM elections
          WHERE position = ? AND reminder_sent_date IS NOT NULL";

        $query = $this->db->query($sql, array($position));
        
        $result = array();
        if ($query->num_rows() > 0) {
          foreach ($query->result() as $row) {
            $result[0] = TRUE;
            $result['position'] = $row->position;
            $result['reminder'] = $row->reminder_sent_date;            
            }
          }
          else $result = FALSE;
          
            return $result;
}

    //returns information like vote count for on going elections
    //this includes information about writein candidates
    // write in candidates are stored in:
    //    ['writein_email'] and ['writein_count']
    public function view_election_votes($position = "") {
        if ($position == "")
            $position = $this->input->post('position');


        $sql = "SELECT u.first_name, u.last_name, cp.vote_number, cp.position 
            FROM users u, candidate_positions cp, elections e 
            WHERE  e.position = ? AND e.position = cp.position AND cp.email = u.email";
        $query = $this->db->query($sql, array($position));

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['first_name'][] = $row->first_name;
            $result['last_name'][] = $row->last_name;
            $total_votes += (int) $row->vote_number;
            $result['vote_number'][] = $row->vote_number;
            $result['position'][] = $row->position;
        }


        //get all write in candidates and their vote count
        $sql = "SELECT w.email, w.vote_count
            FROM write_in_candidate w
            WHERE w.position = ?";
        $query = $this->db->query($sql, array($position));

        foreach ($query->result() as $row) {
            $result['writein_email'][] = $row->email;
            $result['writein_count'][] = $row->vote_count;
            $total_votes += (int) $row->vote_count;
        }
        $result['total_votes'] = $total_votes;
        return $result;
    }

    //declare the election results as valid and final
    public function finalize_election_results($position = "") {
        if ($position == "")
            $position = $this->input->post('position');

        $final = array(
            'final_result' => 1
        );


        $this->db->where('position', $position);
        $update = $this->db->update('elections', $final);

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //returns the name of the candidates and the number of votes per candidate for a given election
    //only returns results for elections that are considered over
    public function get_finished_election_results($position = "") {
        if ($position == "")
            $position = $this->input->post('position');


        $sql = "SELECT u.first_name, u.last_name, cp.vote_number, cp.position 
            FROM users u, candidate_positions cp, elections e 
            WHERE  e.election_over <= NOW() AND e.position = ? AND e.position = cp.position AND cp.email = u.email AND e.final_result = 1";
        $query = $this->db->query($sql, array($position));

        $result = array();
        (int) $total_votes = 0;
        foreach ($query->result() as $row) {
            $result['first_name'][] = $row->first_name;
            $result['last_name'][] = $row->last_name;
            $total_votes += (int) $row->vote_number;
            $result['vote_number'][] = $row->vote_number;
            $result['position'][] = $row->position;
        }

        //get all write in candidates and their vote count
        $sql = "SELECT w.email, w.vote_count
            FROM write_in_candidate w
            WHERE w.position = ?";
        $query = $this->db->query($sql, array($position));

        foreach ($query->result() as $row) {
            $result['writein_email'][] = $row->email;
            $result['writein_count'][] = $row->vote_count;
            $total_votes += (int) $row->vote_count;
        }
        $result['total_votes'] = $total_votes;

        return $result;
    }

    //creates a new election and adds it to the database    
    //the admin_email param is empty string by default ""
    //if the param is left as empty string the function will use the post array for the email
    //POST INFORMATION
    // election name is a string of 50
    // description string of 250
    // eligibility string of 300
    // registration and election start / over are of type datetime
    // data times must be in the format of "yyyy-mm-dd hh:mm:ss", example: "2014-10-01 09:32:15"
    public function create_new_election($position = "") {
        if ($position == "")
            $position = $this->input->post('election_name');

        //check to see if the entry already exists
        $this->db->where('position', $position); //prepare the sql statement
        $result = $this->db->get('elections'); //pick the table to select form        
        if ($result->num_rows() > 0)
            return FALSE; //psotion already exists
        else {

            //insert the user information into the user table
            //create the user account
            $create_election = array();
            $create_election = array(
                'position' => $position,
                'description' => $this->input->post('description'),
                'eligibility' => $this->input->post('eligibility'),
                'registration_start' => $this->input->post('registration_start'),
                'registration_over' => $this->input->post('registration_over'),
                'election_start' => $this->input->post('election_start'),
                'election_over' => $this->input->post('election_over')
            );

            $insert = $this->db->insert('elections', $create_election);

            return $insert;
        }
    }

    //allow the user the requset a re election
    public function reelection_request($position = "") {
        if ($position == "")
            $position = $this->input->post('position');

        $this->db->where('position', $position);
        $this->db->set('reelection_requested', '1');
        $this->db->update('elections');

        if ($this->db->affected_rows() > 0)
            return TRUE;
        else
            return FALSE;
    }

    //remove the election
    //this will only work if the election is already over
    //also removes all the candidates that were running for the election
    public function remove_election($position = "") {
        if ($position == "")
            $position = $this->input->post('position');

        $this->db->where('position', $position);
        if ($this->db->delete('elections') === FALSE)
            return FALSE;

        if ($this->db->affected_rows() > 0)
            $tempA = TRUE;
        else
            $tempA = FALSE;

        $this->db->where('position', $position);
        $this->db->delete('candidate_positions');
          

      

        //find all the candidates that are not in the candidate postions table
        //this will return all candidates that are not running for any positions anymore
        $sql = "SELECT c.email FROM candidate c WHERE c.email NOT IN (SELECT email FROM candidate_positions)";
        $query = $this->db->query($sql);

        //the candidate is not running for anythign anymore remove them from the candidates list
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $this->db->where('email', $row->email);
                $this->db->delete('candidate');
                    
            }
        }
     

        //delete write in candidates
        $this->db->where('position', $position);
        $this->db->delete('write_in_candidate');

        if ($tempA === TRUE)
            return TRUE;
        else
            return FALSE;
    }
    
    //returns an array with index 0-23 of how many votes there where per hour for the past 24 hours
    //index 0 is the number of votes that happend <1 hr ago
    // index 1 is number of votes that happend 1-2 hrs ago 
    // and so forth where index 23 is the number of votes for 24 hours ago
    public function get_hourly_results($position = ""){
      if ($position == "")
            $position = $this->input->post('position');
            
      date_default_timezone_set('America/Chicago');
      
            $sql = "SELECT vote_date
            FROM voter 
            WHERE position = ? AND voted = '1' AND approved = '1'";
            
            $query = $this->db->query($sql,array($position));
                
            $count = array(); 
            $count = array_fill(0,24,0);
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {                                
                
                    $currentDate = new DateTime(date("Y-m-d H:i:s"));
                    $votedDate = new DateTime($row->vote_date);
                    $interval = $currentDate->diff($votedDate);

                    if($interval->y == 0 || $interval->m == 0 || $interval->d == 0 ) 
                      $count[$interval->h]++;                
                }
            }
            return $count;
    }

}

?>
