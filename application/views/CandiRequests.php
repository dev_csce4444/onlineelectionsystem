
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>OES Election Manager</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">
    
    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/flat-ui.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/template.css') ?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/html5shiv.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/respond.min.js'); ?> "></script>
    <![endif]-->
  </head>

  <body>

    <!--Login Modal -->


    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
<li><?php echo anchor('ApprovalMgr/loadApprovedVoters', 'Approved Voters'); ?> <!-- <a href="ApprovedVoters.php">Approved Voters</a> --> </li>
<li><?php echo anchor('ApprovalMgr/loadApprovedCandis', 'Approved Candidates'); ?> <!-- <a href="ApprovedVoters.php">Approved Voters</a> --> </li>
<li><?php echo anchor('ApprovalMgr/loadVoterReqs', 'Voter Requests'); ?> <!-- <a href="ApprovedVoters.php">Approved Voters</a> --> </li>
<li><?php echo anchor('ApprovalMgr/loadCandisReqs', 'Candidate Requests'); ?> <!-- <a href="ApprovedVoters.php">Approved Voters</a> --> </li>
<li><?php echo anchor('ApprovalMgr/AmgrResults', 'View Results/Statistics'); ?> <!-- <a href="ApprovedVoters.php">Approved Voters</a> --> </li>

        </ul>
        <br/>

    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg'); ?>" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                        <!-- <div id="logo"></div> -->
                    </div>
									
					<!-- Will be show by default -->
                    <div id="login-text" class="show col-xs-2"> 
						<div class="row  pull-right account-text">
							<div class="col-sm-6 col-md-5 col-lg-4">
								<?php echo anchor('login/logout','Logout'); ?>
							</div>
                    	</div>
					</div>
					
					<!-- User account drop down. Only visible when logged in -->

					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/banner8.jpg'); ?>);">
                        <h1 class="uppercase" id="banner-text">Candidate Requests</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->
        <div class="container">
            <div class="row">

                    <div class="col-xs-12 col-md-8 div-center">
					<?php
						
                        echo "<br>";
                        echo "<ul class=\"list-group\">";
                        try{
                        if(isset($first_name))
                        {
                        for($i=0;$i<sizeof($first_name);$i++)
                        {
                            
                            echo "<li class=\"list-group-item\">" .$first_name[$i]." ".$last_name[$i]."<br/> College: ".$college[$i]."<br/>Running For: ".$position[$i]."<br/>Major: ".$major[$i]."<br/>UNT ID: ".$untID[$i]."";
                            
                            echo form_open('ApprovalMgr/AprCandi/')."<input type=\"hidden\" name=\"email\" value=\"".$email[$i]."\" /><input type=\"hidden\" name=\"position\" value=\"".$position[$i]."\" />".form_submit($i, 'Approve' )."</form>";
                            
                            
                            
                            echo form_open('ApprovalMgr/DisCandi/')."<input type=\"hidden\" name=\"email\" value=\"".$email[$i]."\" /><input type=\"hidden\" name=\"position\" value=\"".$position[$i]."\" />".form_submit($i, 'Disapprove' )."</form>";
                            
                            echo "" . "</li>";
                            
                        }
                        }
                        else
                            echo "<li>No Candidates</li>";
                        }
                        catch (Exception $E)
                        {
                            echo "<li>No Candidates</li>";
                        }
                        echo "</ul>";
					
                    ?>
					</div>

            </div>
        </div>

        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made with love at University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js'); ?>"></script>
	
	
  
  </body>
  </body>

</html>
