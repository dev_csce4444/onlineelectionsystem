<!DOCTYPE html>
<html lang="en">
<head>
<title>UNT Voter Booth - Apply to be a Voter </title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link rel="icon" type="image/ico" href="<?php echo base_url('assets/images/favicon.ico') ?>">

<!-- Loading Bootstrap, Flat UI and custom site template -->
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/flat-ui.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/template.css') ?>" rel="stylesheet">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
             <script src="https://code.jquery.com/jquery.js"></script>
             <!-- Include all compiled plugins (below), or include individual files as needed -->
             <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
             <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js') ?>"></script>
             
             
             <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
             <!--[if lt IE 9]>
             <script src="<?php echo base_url('assets/js/html5shiv.js') ?>"></script>
             <script src="<?php echo base_url('assets/js/respond.min.js') ?>"></script>
             <![endif]-->
             </head>


<body>


<!-- Hidden Menu -->
<nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
<p class="white uppercase" id="hidden-menu-header">Menu</p>
<ul class="nav navmenu-nav uppercase">
<li><p class="white uppercase" > <?php echo anchor('voter/index', 'Vote for an Election', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterProfiles', 'Browse Profiles', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterElectionResults', 'View Results/Statistics', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterCreateProfile', 'Change my Password', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/Voterapplyvoter', 'Apply to be a Voter', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/Voterapplycandi', 'Apply to be a Candidate', 'title="reg"');  ?> </p></li>
</ul>
<br/>
</nav>

<div id="wrap">
<header>
<div class="container-fluid">

<!-- Main header menu starts here -->
<div class="row" id="header-top">
             <div class="col-xs-2">
             <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg') ?>" alt="Menu"/></a>
             </div><div class="col-xs-8">
<!--           <div id="logo"></div>   -->
</div>
             <div id="login-text" class="show col-xs-2">
             <div class="row  pull-right account-text">
             <div class="col-sm-6 col-md-5 col-lg-4">
             <?php echo anchor('login/logout','Logout'); ?>
             </div>
             </div>
             </div>
<!-- User account drop down. Only visible when logged in -->
<div id="account-button" class="hidden col-xs-2">
<div class="dropdown pull-right">
<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['name']; ?><span class="caret"></span></button>
<span class="dropdown-arrow"></span>
<ul class="dropdown-menu">
<li><a href="account.html">Account</a></li>
<li><a href="my-posts.php">My posts</a></li>
<li><a href="logout.php" id="logout">Logout</a></li>
</ul>
</div>
</div>
<!-- End of Main header menu -->
</div>

<!-- Banner section starts here -->
<div class="row">
             <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/home-bg.jpg') ?>) ;">
             <h1 class="uppercase" id="banner-text">Apply to be a Voter</h1> <!-- CHANGE THIS -->
</div>
</div>
<!-- End of Banner section -->
</div>
</header>

<!-- Page content starts here -->
<div class="container">
<div class="row">
<br/><br/>
<div class="col-xs-12 col-sm-7 col-lg-5 div-center">
   
    
    <label><strong class="uppercase">Please select the election to be a voter</strong></label> 
    <?php
    echo validation_errors();
     echo "<br>";
                        echo "<ul class=\"list-group\">";
                        try{
                        if(isset($first_name))
                        {
    //connet the form to the create account function for validation
    echo form_open('voter/Voterapplyvotersumbit');?>
     <?php

     for($i=0;$i<count($position);$i++)
     $election[$position[$i]] = $position[$i];
    
    echo form_dropdown('election', $election, 'none'); ?>

        <br/>
        <br/>
        <br/>
    <div class="btn btn-default btn-lg">
   <?php echo form_submit('submit', 'Apply');
    ?>
    </div>
</form>
<?
  }
                        else
                            echo "<li>No Elections</li>";
                        }
                        catch (Exception $E)
                        {
                            echo "<li>No Elections</li>";
                        }
                        echo "</ul>";
?>
</div>
</div>
</div>


<div id="push"></div> <!-- This pushes the footer to the bottom -->

</div>

<!-- footer starts here -->
<footer>
<p id="footer-text">Made at the University of North Texas</p>
</footer>

             
             </body>
             
             </html>