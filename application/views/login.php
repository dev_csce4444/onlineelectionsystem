
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>UNT Voter Booth</title>



    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">




    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/landing-page.css'); ?>" rel="stylesheet">




    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">UNT Voter Booth</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Header -->
    <div class="intro-header">

        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>UNT Voter Booth</h1>
                        <h3>An Online Election System</h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                        
                    <?php echo form_open('login/validate_login'); ?>
			         <div>
						<span><label>Username</label></span>
                    <span><input type="text" name="email" style="color:black" >
                    </span>
					 </div>
					 <div>
						<span><label>Password</label></span>
                        <span>
                        <input name="password" type="password" style="color:black"></span>
					 </div>
					<div class="sign">
						<span class="forget-pass">
                        <?php echo anchor('login/loadfpswd/', 'Forgot Password?');?>
                        </span>
							<div class="clear"> </div>
					</div>
						<br/>
                            <li>
                            <div >
                            <input class="btn btn-default btn-lg" type="submit" value="Login" />

                            </div>


                            </li>
							<br/>
                            <li>
                            <div class="btn btn-default btn-lg">
                            <?php echo anchor('login/signup', 'Register');?>
                            </div>
                            </li>
                            <!-- <li>
                                <div class="btn btn-default btn-lg">
                                <?php echo anchor('Admin/index', 'Head Admin'); ?>
                                </div></li><li>
                                <div class="btn btn-default btn-lg">
                                <?php echo anchor('ApprovalMgr/loadApprovedVoters', 'Approval Manager'); ?>
                                </div>
                            </li>
                            <li>
                            <div class="btn btn-default btn-lg">
                            <?php echo anchor('ElectionMgr/loadElectionManager', 'Election Manager'); ?>
                            </div>
                            </li>
<li>
<div class="btn btn-default btn-lg">
<?php echo anchor('candidate/index', 'Candidate'); ?>
</div>
</li>
<li>
<div class="btn btn-default btn-lg">
<?php echo anchor('voter/index', 'Voter'); ?>
</div>
</li>
<li>
<div class="btn btn-default btn-lg">
<?php echo anchor('ElectionMtr/loadMtrCurElections', 'Election Monitor'); ?>
</div>
</li> -->

                        </ul>
                       </form>

                    </div>
                </div>
            </div>
            
        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    

    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url('assets/js/jquery-1.11.0.js');?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>

</body>

</html>