<!DOCTYPE html>
<html lang="en">
  <head>
    <title>OES Add Election</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">

    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/flat-ui.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/template.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/html5shiv.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
    <![endif]-->
  </head>

  <body>

    <!--Login Modal -->



    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
<li><?php echo anchor('ElectionMgr/loadElectionManager', 'Upcoming Elections'); ?></li>
<li><?php echo anchor('ElectionMgr/loadCurrentElections', 'Current Elections'); ?></li>
<li><?php echo anchor('ElectionMgr/loadCompletedElections', 'Completed Elections'); ?></li>
<li><?php echo anchor('ElectionMgr/loadAddElection', 'Add Election'); ?></li>
<li><?php echo anchor('ElectionMgr/EMgrResults', 'View Results/Statistics'); ?></li>
        </ul>
        <br/>

    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg'); ?>" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                        <!--<div id="logo"></div>-->
                    </div>

					<!-- Will be show by default -->
                    <div id="login-text" class="show col-xs-2"> 
						<div class="row  pull-right account-text">
							<div class="col-sm-6 col-md-5 col-lg-4">
<?php echo anchor('login/logout','Logout'); ?>
</div>
                    	</div>
					</div>

					<!-- User account drop down. Only visible when logged in -->

					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/banner7.jpg'); ?>);">
                        <h1 class="uppercase" id="banner-text">Declare Election Results</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->
        <div class="container">
            <div class="row">
				
				<br>
				
               <div class="col-xs-12 col-md-8 div-center">
                <?php
                 echo "<br>";
                        echo "<ul class=\"list-group\">";
                        try{
                        if(isset($position))
                        {
                
                echo form_open('ElectionMgr/declareElection/'.$position); ?>
                <strong class="uppercase">Election Name</strong>
                    <?php echo $position; ?>
					<br>

					<strong class="uppercase">Description</strong>
                    <?php echo $description; ?>
					<br>

					<strong class="uppercase">Eligible voters</strong>
                    <?php echo $eligibility; ?>
					<br>

                    <strong class="uppercase">Election Results</strong>
                    <?php
                        for ($i=0;$i<sizeof($email);$i++){
                            
                            echo $email[$i]." : ".$vote_number[$i];
                            
                        }
                    ?>
                    <br>

					<strong class="uppercase">Feedback From Monitor </strong>
                    <?php echo $feedback; ?>
					<br>

					<div class="div-center text-center">
						<div class="form-actions">
						<button class="btn btn-info btn-lg" input type="submit">Declare</button>
						</div>
					</div>

				</form>
  <?  }
                        else
                            echo "<li>No Elections</li>";
                        }
                        catch (Exception $E)
                        {
                            echo "<li>No Elections</li>";
                        }
                        echo "</ul>";
               ?>
<div class="div-center text-center">
<div class="btn btn-default btn-lg">
<?php echo anchor('ElectionMgr/loadEditElection/'.$position,'Re-Election'); ?>
</div>
</div>
<br/>
<div class="div-center text-center">
<div class="btn btn-default btn-lg">
<?php echo anchor('ElectionMgr/loadEditElection/'.$position,'Runoff'); ?>
</div>
</div>
				   <br>
		</div>
            </div>
        </div>

        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made with love at University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.tagsinput.js'); ?>"></script>
	  <script src="<?php echo base_url('assets/js/flatui-radio.js'); ?>"></script>
	<script type='text/javascript'>
		$(".tagsinput").tagsInput();
	</script>

	
  </body>

</html>

