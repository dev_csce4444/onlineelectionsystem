<!DOCTYPE html>
<html lang="en">
  <head>
    <title>OES Add Election</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">

    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/flat-ui.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/template.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/html5shiv.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
    <![endif]-->
  </head>

  <body>


    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
<li><?php echo anchor('ElectionMgr/loadElectionManager', 'Upcoming Elections'); ?></li>
<li><?php echo anchor('ElectionMgr/loadCurrentElections', 'Current Elections'); ?></li>
<li><?php echo anchor('ElectionMgr/loadCompletedElections', 'Completed Elections'); ?></li>
<li><?php echo anchor('ElectionMgr/loadAddElection', 'Add Election'); ?></li>
<li><?php echo anchor('ElectionMgr/EMgrResults', 'View Results/Statistics'); ?></li>
        </ul>
          </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg'); ?>" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                        <!--<div id="logo"></div>-->
                    </div>

					<!-- Will be show by default -->
                    <div id="login-text" class="show col-xs-2"> 
						<div class="row  pull-right account-text">
							<div class="col-sm-6 col-md-5 col-lg-4">
								<?php echo anchor('login/logout','Logout'); ?>
							</div>
                    	</div>
					</div>


					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/banner7.jpg'); ?>);">
                        <h1 class="uppercase" id="banner-text">Add Election</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->
        <div class="container">
            <div class="row">
				
				<br>
				
               <div class="col-xs-12 col-md-8 div-center">
				<?php echo form_open('ElectionMgr/AddElection'); ?>
					<strong class="uppercase">Election Name</strong>
					<input type="text" class="form-control" name="election_name" placeholder="Name of the Election" required>

					<br>									

					<strong class="uppercase">Description</strong>
					<textarea class="form-control" name="description" rows="5" placeholder="Provide a little bit of information about the election" required></textarea>

					<br>

					<strong class="uppercase">Eligible voters</strong>
					<input type="text" class="form-control" name="eligibility" placeholder="Eligibility Criteria" required>

					<br>

                    <strong class="uppercase">Registration Begins</strong>
                    <input type="text" class="form-control" name="registration_start" placeholder="YYYY-MM-DD" required>

                    <br>

					<strong class="uppercase">Registration Ends </strong>
					<input type="text" class="form-control" name="registration_over" placeholder="YYYY-MM-DD" required>

					<br>

                    <strong class="uppercase">Election Begins</strong>
                    <input type="text" class="form-control" name="election_start" placeholder="YYYY-MM-DD" required>

                    <br>

                    <strong class="uppercase">Election Ends</strong>
                    <input type="text" class="form-control" name="election_over" placeholder="YYYY-MM-DD" required>

                    <br>

					<div class="div-center text-center">
						<div class="form-actions">
						<button class="btn btn-info btn-lg" input type="submit">Create Election</button>
						</div>
					</div>
				</form>
				   
				   <br>
		</div>

            </div>
        </div>

        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made with love at University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery.tagsinput.js'); ?>"></script>
	  <script src="<?php echo base_url('assets/js/flatui-radio.js'); ?>"></script>
	<script type='text/javascript'>
		$(".tagsinput").tagsInput();
	</script>

	
  </body>

</html>

