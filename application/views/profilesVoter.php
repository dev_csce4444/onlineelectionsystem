<!DOCTYPE html>
<html lang="en">
  <head>
    <title>UNT Voter Booth - Profiles</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">
    
    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/flat-ui.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/template.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/html5shiv.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/respond.min.js');?>"></script>
    <![endif]-->
    <style>
        .inactive-text{
            color: #c6c6c6;
        }

        .text-left{
            text-align: left;
        }
    </style>
  </head>

  <body>


    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
<li><p class="white uppercase" > <?php echo anchor('voter/index', 'Vote for an Election', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterProfiles', 'Browse Profiles', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterElectionResults', 'View Results/Statistics', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterCreateProfile', 'Change my Password', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/Voterapplyvoter', 'Apply to be a Voter', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/Voterapplycandi', 'Apply to be a Candidate', 'title="reg"');  ?> </p></li>
        </ul>
    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg');?>" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                 <!--    <div id="logo"></div>    -->    
                    </div>
<div id="login-text" class="show col-xs-2">
<div class="row  pull-right account-text">
<div class="col-sm-6 col-md-5 col-lg-4">
<?php echo anchor('login/logout','Logout'); ?>
</div>
</div>
</div>
					<!-- User account drop down. Only visible when logged in -->
					<div id="account-button" class="hidden col-xs-2">
                        <div class="dropdown pull-right">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['name']; ?><span class="caret"></span></button>
                            <span class="dropdown-arrow"></span>
                            <ul class="dropdown-menu">
                                <li><a href="account.html">Account</a></li>
                                <li><a href="my-posts.php">My posts</a></li>
                                <li><a href="logout.php" id="logout">Logout</a></li>
                            </ul>
                        </div>
                    </div>
					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/home-bg.jpg'); ?>);">
                        <h1 class="uppercase" id="banner-text">Browse Candidate Profiles</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->
        <div class="container">

            <br/><br/>
           <?  echo "<br>";
                        echo "<ul class=\"list-group\">";
                        try{
                        if(isset($first_name))
                        {
            ?>
            <h4>Candidates</h4>


            <form method="POST" action = <?php echo site_url("voter/voterProfilesMore")?>>               
            <?php
            //goes through the database and pulls all the candiates and pulls the previous held positions
            //echo form_open("voter/voterProfilesMore");
            for($i = 0; $i < sizeof($email);$i++)
                
            { ?>
            <div class="row">
                <div class="col-md-3 text-center profile-box">
                    <div class="well">
                        <h6><?php echo $first_name[$i]; ?></h6>
                        <?php echo " "; ?>
                        <h6><?php echo $last_name[$i]; ?></h6>
                        <div class="text-left">
                            <strong>Positions Held: </strong>
                             <p><?php echo $held_positions[$i]; ?></p>
                        </div>
                        
                            <input type="submit" class="btn btn-danger" name="submit" value="More..">
                        
                    </div>
                </div>
            </div>

            <?php 

                  echo form_hidden('first_name',$first_name); 
                  echo form_hidden('last_name',$last_name);
                  echo form_hidden('held_positions',$held_positions);
                  echo form_hidden('bio',$bio); 
                  echo form_hidden('year',$year); 
                  echo form_hidden('major',$major);
                  echo form_hidden('email',$email);


            ?>
            
            <?php }
            //echo form_close();
            
             ?>
        </form> 

         <?
           }
                        else
                            echo "<li>No Candidates</li>";
                        }
                        catch (Exception $E)
                        {
                            echo "<li>No Candidates</li>";
                        }
                        echo "</ul>";
         ?>

            <br/><br/>
        </div>

        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made at the University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-select.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.tagsinput.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.placeholder.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/application.js'); ?>"></script>


  </body>

</html>