<!DOCTYPE html>
<html lang="en">
  <head>
    <title>UNT Voter Booth - Elections</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">
    
    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/flat-ui.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/template.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/html5shiv.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
    <![endif]-->

    <style>
        .inactive-text{
            color: #c6c6c6;
        }

        .text-right{
            text-align: right;
        }
    </style>
  </head>

  <body>

     <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
<li><?php echo anchor('Admin/loadAddAdmin', 'Add Admin', 'title="reg"'); ?> <!--<a href="AddAdmin.php">Add Admin</a> --> </li>
<li> <?php echo anchor('Admin/index', 'Edit Admin Panel', 'title="reg"'); ?> <!-- <a href="HeadAdmin.php">View/Edit Admin Panel</a> --></li>
<li> <?php echo anchor('Admin/loadHAProfilesBrowse', 'Browse Profiles', 'title="reg"'); ?> <!--<a href="HAElectionResults.php">View Election Results</a> --> </li>
<li><?php echo anchor('Admin/HAElectionResults', 'View Results', 'title="reg"'); ?><!-- <a href="profilesHAdmin.php">Browse Profiles</a>--> </li>
</ul>
    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg'); ?>" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                    <!--     <div id="logo"></div>  --> 
                    </div>
<div id="login-text" class="show col-xs-2">
<div class="row  pull-right account-text">
<div class="col-sm-6 col-md-5 col-lg-4">
<?php echo anchor('login/logout','Logout'); ?>
</div>
</div>
</div>
					<!-- User account drop down. Only visible when logged in -->
					<div id="account-button" class="hidden col-xs-4 col-sm-2">
						<div class="dropdown pull-right">
					    	<button class="btn btn-default dropdown-toggle" data-toggle="dropdown">First name<span class="caret"></span></button>
					  		<span class="dropdown-arrow"></span>
					  		<ul class="dropdown-menu">
								<li><a href="#">Account</a></li>
								<li><a href="#">Messages</a></li>
								<li><a href="#">Logout</a></li>
					  		</ul>
						</div>
					</div>
					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/home-bg.jpg'); ?>);">
                        <h1 class="uppercase" id="banner-text">Election Results</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->
        <div class="container">
            <div class="row">
              <br/>
                <!-- <div class="col-md-3">
                    <div class="form-group">
                        <div class="input-group">                               
                            <input type="text" class="form-control" placeholder="Search elections" id="searchElections">
                            <span class="input-group-btn">
                                <button type="submit" class="btn"><span class="fui-search"></span></button>
                            </span>
                        </div>
                    </div>

                </div> -->
                
            <?php
             echo "<br>";
                        echo "<ul class=\"list-group\">";
                        try{
                        if(isset($position))
                        {
            //pulls the completed elections from the database and shows the votes recieved and place
            for($i = 0; $i < sizeof($position);$i++)
            { ?>
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-xs-12  well">
                                <h6>Election: <?php echo $position[$i]; ?>                                
                                <h6>Description: <?php echo $description[$i]; ?></h6>
                                <h6>Eligibility: <?php echo $eligibility[$i]; ?></h6>
                                <h6>Election Ended: <?php echo $election_over[$i]; ?></h6>
                                <div class="row">
                                <div class="col-xs-8">  
                                </div>
                                <div class="col-xs-4">
                                    <div class="btn btn-danger pull-right">
                                    <?  
                                    echo form_open('voter/viewVoterResults');
                                    echo form_hidden('position',$position[$i]);
                                    echo form_submit('submit','View Results/Statistics');
                                    echo form_close();
                                      ?>  
                                    </div>
                                    
                                </div>
                                <br/>
                            </div>    
                        </div>
                    </div>

                </div>
            <?php }
            
              }
                        else
                            echo "<li>No Elections</li>";
                        }
                        catch (Exception $E)
                        {
                            echo "<li>No Elections</li>";
                        }
                        echo "</ul>";
            ?>


            </div>


        </div>

        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made at University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-select.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.tagsinput.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.placeholder.js');?>"></script>
    <script src="<?php echo base_url('assets/js/application.js'); ?>"></script>
  </body>

</html>