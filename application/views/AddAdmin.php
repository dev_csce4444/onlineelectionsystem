<!DOCTYPE html>
<html lang="en">
  <head>
    <title>OES Add Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">
    
    <!-- Loading Bootstrap, Flat UI and custom site template -->
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/flat-ui.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/template.css'); ?>" rel="stylesheet">

<!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
<!--[if lt IE 9]>
<script src="<?php echo base_url('assets/js/html5shiv.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
<![endif]-->

<style>
.img-circle{
    -moz-box-shadow: 0px 0px 0px 5px #e8e6e6; /* Firefox */
    -webkit-box-shadow: 0px 0px 0px 5px #e8e6e6; /* Safari, Chrome */
		 	box-shadow: 0px 0px 0px 5px #e8e6e6; /* CSS3 */
}

label{
cursor: pointer;
}
</style>


  </head>

  <body>


    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
<li><?php echo anchor('Admin/loadAddAdmin', 'Add Admin', 'title="reg"'); ?> <!--<a href="AddAdmin.php">Add Admin</a> --> </li>
<li> <?php echo anchor('Admin/index', 'Edit Admin Panel', 'title="reg"'); ?> <!-- <a href="HeadAdmin.php">View/Edit Admin Panel</a> --></li>
<li> <?php echo anchor('Admin/loadHAProfilesBrowse', 'Browse Profiles', 'title="reg"'); ?> <!--<a href="HAElectionResults.php">View Election Results</a> --> </li>
<li><?php echo anchor('Admin/HAElectionResults', 'View Results', 'title="reg"'); ?><!-- <a href="profilesHAdmin.php">Browse Profiles</a>--> </li>
</ul>
        <br/>
           </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg'); ?>" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                        <!--<div id="logo"></div> -->
                    </div>
									
					<!-- Will be show by default -->
                    <div id="login-text" class="show col-xs-2"> 
                        <div class="row  pull-right account-text">
                            <div class="col-sm-6 col-md-5 col-lg-4">
                                <?php echo anchor('login/logout','Logout'); ?>
                            </div>
                        </div>
                    </div>
					
					<!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/banner8.jpg'); ?>);" >
                        <h1 class="uppercase" id="banner-text">Add Admin</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->
        <div class="container">
            <div class="row">
				<br/><br/>
				<div class="col-xs-12 col-sm-7 col-lg-5 div-center">
					<?php echo form_open('Admin/AddAdmin'); ?>	
						
						<label><strong class="uppercase">Registered Email</strong></label>
						<div class="form-group col-sm-10 div-center">
							<div class="input-group">
								<span class="input-group-addon"><span class="input-icon fui-mail"></span></span>
								<input type="email" class="form-control" name="emailAddress" placeholder="Email address" required>
							</div>
						</div>
						
						<br/>

                        <strong class="uppercase">Role</strong>
                        <div class="row">
                            <div class="col-xs-12">
                             <input type="radio" name="group1" value="HeadAdmin" data-toggle="radio" /> Head Admin <br/>
                             <input type="radio" name="group1" value="ElectionManager" data-toggle="radio" /> Election Manager <br/>
                             <input type="radio" name="group1" value="ElectionMonitor" data-toggle="radio" /> Election Monitor <br/>
                             <input type="radio" name="group1" value="ApprovalManager" data-toggle="radio" /> Approval Manager <br/>

                            </div>
                        </div>

                        <br/>



						<div class="form-actions">
					  		<button type="submit" class="btn btn-info">Create Admin</button>
						</div>
					</form>
				</div>
                <br/>
            </div>
        </div>

        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made with love at University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js'); ?>"></script>

    <script type="text/javascript">
        uname = "<?php echo $_SESSION['name'];?>";
        
        if (uname!=("")){//indicaates that no one is logged in
            $('#account-button').toggleClass('hidden');
            $('#login-text').toggleClass('hidden');
        }
    </script>
  </body>

</html>
