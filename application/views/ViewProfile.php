<!DOCTYPE html>
<html lang="en">
<head>
<title>UNT Voter Booth - Create an Account</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="icon" type="image/ico" href="<?php echo base_url('assets/images/favicon.ico') ?>">

<!-- Loading Bootstrap, Flat UI and custom site template -->
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/flat-ui.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/template.css') ?>" rel="stylesheet">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
             <script src="https://code.jquery.com/jquery.js"></script>
             <!-- Include all compiled plugins (below), or include individual files as needed -->
             <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
             <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js') ?>"></script>
             
             
             <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
             <!--[if lt IE 9]>
             <script src="<?php echo base_url('assets/js/html5shiv.js') ?>"></script>
             <script src="<?php echo base_url('assets/js/respond.min.js') ?>"></script>
             <![endif]-->
             </head>

  <body>


    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">
<li><p class="white uppercase" > <?php echo anchor('voter/index', 'Vote for an Election', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterProfiles', 'Browse Profiles', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterElectionResults', 'View Results/Statistics', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/voterCreateProfile', 'Change my Password', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/Voterapplyvoter', 'Apply to be a Voter', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('voter/Voterapplycandi', 'Apply to be a Candidate', 'title="reg"');  ?> </p></li>
             </ul>
    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
             <div class="row" id="header-top">
             <div class="col-xs-2">
             <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg') ?>" alt="Menu"/></a>
             </div>
             <div class="col-xs-8">
             <!--           <div id="logo"></div>   -->
             </div>
             <div id="login-text" class="show col-xs-2">
             <div class="row  pull-right account-text">
             <div class="col-sm-6 col-md-5 col-lg-4">
             <?php echo anchor('login/logout','Logout'); ?>
             </div>
             </div>
             </div>
             <!-- User account drop down. Only visible when logged in -->
             <div id="account-button" class="hidden col-xs-2">
             <div class="dropdown pull-right">
             <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['name']; ?><span class="caret"></span></button>
             <span class="dropdown-arrow"></span>
             <ul class="dropdown-menu">
             <li><a href="account.html">Account</a></li>
             <li><a href="my-posts.php">My posts</a></li>
             <li><a href="logout.php" id="logout">Logout</a></li>
             </ul>
             </div>
             </div>
             <!-- End of Main header menu -->
             </div>
             
             <!-- Banner section starts here -->
             <div class="row">
             <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/home-bg.jpg') ?>) ;">

                        <h1 class="uppercase" id="banner-text">Profiles</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
				<!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here --> 

                                     
        <div class="container">
            <div class="row" id="content">
					<br>
                                        <?
                                           echo "<br>";
                        echo "<ul class=\"list-group\">";
                        try{
                        if(isset($first_name))
                        {?>
                    <div class="col-xs-12 col-md-8 div-center">
						<div class ="row text-center">
						
	<?php
       
        echo $first_name; 
                             echo " "; 
                           echo $last_name; 
                        
                        ?>
						</div>
						<br>
						<div class ="row">
							<div class="col-xs-12 div-center text-center"> 
												
								
								<strong class="uppercase">Major</strong>
								<p>
								<?php echo $major; ?>
								</p>
								
								<strong class="uppercase">Classification</strong>
								<p>
								<?php echo $year; ?>
								</p>
								
								<strong class="uppercase">Bio</strong>
								<p>
								<?php echo $bio; ?>
								</p>
								
								<strong class="uppercase">Previously Held Positions</strong>
								<p>
								<?php echo $held_positions; ?>
								</p>
								
							</div>
						</div>
						<br>
					</div>
                                        <?    }
                        else
                            echo "<li>No Profile</li>";
                        }
                        catch (Exception $E)
                        {
                            echo "<li>No Profile</li>";
                        }
                        echo "</ul>";?>


					
        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made at the University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jasny-bootstrap.min.js"></script>
	<script src="js/jquery.tagsinput.js"></script>
	  <script src="js/flatui-radio.js"></script>
	<script type='text/javascript'>
		$(".tagsinput").tagsInput();
	</script>
  </body>
</html>