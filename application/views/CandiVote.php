<!DOCTYPE html>
<html lang="en">
  <head>
    <title>UNT Voter Booth - Vote</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/ico" href="images/favicon.ico">
    
    <!-- Loading Bootstrap, Flat UI and custom site template -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css') ;?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/flat-ui.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/template.css'); ?>" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->
	  
	  <style>
		  .img-circle{
			-moz-box-shadow: 0px 0px 0px 5px #e8e6e6; /* Firefox */  
		  	-webkit-box-shadow: 0px 0px 0px 5px #e8e6e6; /* Safari, Chrome */  
		 	box-shadow: 0px 0px 0px 5px #e8e6e6; /* CSS3 */
		  }
		  
		  label{
			  cursor: pointer;
		  }
	  </style>
  </head>

  <body>


    <!-- Hidden Menu -->
    <nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
        <p class="white uppercase" id="hidden-menu-header">Menu</p>
          <ul class="nav navmenu-nav uppercase">

<li><p class="white uppercase" > <?php echo anchor('candidate/index', 'Vote for an Election', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('candidate/CandiProfiles', 'Browse Profiles', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('candidate/CandiCreateProfile', 'Edit my Profile', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('candidate/viewChangePassword', 'Change my Password', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('candidate/CandiViewProfile', 'View my Profile', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('candidate/CandiElectionResults', 'View Results/Statistics', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('candidate/Candiapplyvoter', 'Apply to be a Voter', 'title="reg"');  ?> </p></li>
<li><p class="white uppercase" > <?php echo anchor('candidate/Candiapplycandi', 'Apply to be a Candidate', 'title="reg"');  ?> </p></li>
       </ul>
    </nav>

    <div id="wrap">
        <header>
            <div class="container-fluid">

                    <!-- Main header menu starts here -->
                <div class="row" id="header-top">
                    <div class="col-xs-2"> 
                        <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg'); ?>" alt="Menu"/></a>
                    </div>
                    <div class="col-xs-8"> 
                 <!--       <div id="logo"></div>    --> 
                    </div>
<div id="login-text" class="show col-xs-2">
<div class="row  pull-right account-text">
<div class="col-sm-6 col-md-5 col-lg-4">
<?php echo anchor('login/logout','Logout'); ?>
</div>
</div>
</div>
                    <!-- User account drop down. Only visible when logged in -->
                    <div id="account-button" class="hidden col-xs-2">
                        <div class="dropdown pull-right">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['name']; ?><span class="caret"></span></button>
                            <span class="dropdown-arrow"></span>
                            <ul class="dropdown-menu">
                                <li><a href="account.html">Account</a></li>
                                <li><a href="my-posts.php">My posts</a></li>
                                <li><a href="logout.php">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- End of Main header menu -->
                </div>

                <!-- Banner section starts here -->
                <div class="row">
                    <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/home-bg.jpg');?> );">
                        <h1 class="uppercase" id="banner-text">Vote</h1> <!-- CHANGE THIS -->
                    </div>
                </div>
                <!-- End of Banner section -->
            </div>
        </header>

        <!-- Page content starts here -->        
        <div class="container">
            <div class="row" id="content">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 div-center well">
                            
                            <form method="POST" action = <?php echo site_url("candidate/submitVote")?>> 
                                <?php
                                //echo form_open("voter/voter/submitVote");
                                //loads all the available elections from the database
                                //show the enddate of the election
                                  echo "<br>";
                        echo "<ul class=\"list-group\">";
                        try{
                        if(isset($first_name))
                        {
                                
                                for($i = 0; $i < sizeof($first_name);$i++)
                                { ?>

                            <strong><?php echo $position[$i];?></strong>
                            <p>You may select only one (1) candidate for this position.</p>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="col-xs-12">
                                        <input type="radio" name="email" value = <?php echo $email[$i];?> required="required">
                                          
                                          <?php echo $first_name[$i];?>
                                          <?php echo " ";?>
                                          <?php echo $last_name[$i];?>
                                          <?php echo form_hidden('position',$position); ?>

                                    </div>
                                </div>
                             </div>

                            <?php } 
                                  }
                        else
                            echo "<li>No Candidates</li>";
                        }
                        catch (Exception $E)
                        {
                            echo "<li>No Candidates</li>";
                        }
                        echo "</ul>";
                            ?>




                            <input type="radio" name="email" value = "" required="required">
                            <?php echo "Write-in candidate";?>
                            <!-- <input type="radio" <?php $email ;?> = name ="candidate" value = "" required="required"> -->
                           <!--  <input type="text" name="candidate" value = <?php echo $email ;?>> -->

                             <br>
                             <div class="row">
                                <div class="col-xs-4">

                                    </br>
                                    <a href=""><button class="btn btn-danger" style="margin-top: -15px;" type="submit">Submit Vote</button></a>
                                </div>
                             </div>
                                <br/>
                            </div>

                        </form>    
                        </div>
                    </div>

                </div>
        </div>
                    
        <div id="push"></div> <!-- This pushes the footer to the bottom -->
    
    </div>
    
    <!-- footer starts here --> 
    <footer>
        <p id="footer-text">Made at the University of North Texas</p>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.tagsinput.js'); ?>"></script>
      <script src="<?php echo base_url('assets/js/flatui-radio.js'); ?> "></script>
    <script type='text/javascript'>
        $(".tagsinput").tagsInput();
    </script>
  </body>
</html>