<!DOCTYPE html>
<html lang="en">
<head>
<title>UNT Voter Booth - Create an Account</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link rel="icon" type="image/ico" href="<?php echo base_url('assets/images/favicon.ico') ?>">

<!-- Loading Bootstrap, Flat UI and custom site template -->
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
<link href="<?php echo base_url('assets/bootstrap/css/jasny-bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/flat-ui.css') ?>" rel="stylesheet">
<link href="<?php echo base_url('assets/css/template.css') ?>" rel="stylesheet">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
             <script src="https://code.jquery.com/jquery.js"></script>
             <!-- Include all compiled plugins (below), or include individual files as needed -->
             <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
             <script src="<?php echo base_url('assets/js/jasny-bootstrap.min.js') ?>"></script>
             
             
             <!-- HTML5 shim, for IE6-8 support of HTML5 elements. -->
             <!--[if lt IE 9]>
             <script src="<?php echo base_url('assets/js/html5shiv.js') ?>"></script>
             <script src="<?php echo base_url('assets/js/respond.min.js') ?>"></script>
             <![endif]-->
             </head>


<body>


<!-- Hidden Menu -->
<nav id="hidden-menu" class="navmenu navmenu-inverse navmenu-fixed-left offcanvas" role="navigation">
<p class="white uppercase" id="hidden-menu-header">Menu</p>
<ul class="nav navmenu-nav uppercase">
<li><a href="login/index">Home</a></li>
</ul>
<br/>
</nav>

<div id="wrap">
<header>
<div class="container-fluid">

<!-- Main header menu starts here -->
<div class="row" id="header-top">
             <div class="col-xs-2">
             <a href="#" data-toggle="offcanvas" data-target="#hidden-menu" data-canvas="body"><img src="<?php echo base_url('assets/images/sidemenuicon.svg') ?>" alt="Menu"/></a>
             </div><div class="col-xs-8">
<!--           <div id="logo"></div>   -->
</div>
          
<!-- User account drop down. Only visible when logged in -->
<div id="account-button" class="hidden col-xs-2">
<div class="dropdown pull-right">
<button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['name']; ?><span class="caret"></span></button>
<span class="dropdown-arrow"></span>
<ul class="dropdown-menu">
<li><a href="account.html">Account</a></li>
<li><a href="my-posts.php">My posts</a></li>
<li><a href="logout.php" id="logout">Logout</a></li>
</ul>
</div>
</div>
<!-- End of Main header menu -->
</div>

<!-- Banner section starts here -->
<div class="row">
             <div class="col-xs-12" id="banner" style="background-image: url(<?php echo base_url('assets/images/home-bg.jpg') ?>) ;">
             <h1 class="uppercase" id="banner-text">Create an Account</h1> <!-- CHANGE THIS -->
</div>
</div>
<!-- End of Banner section -->
</div>
</header>

<!-- Page content starts here -->
<div class="container">
<div class="row">
<br/><br/>
<div class="col-xs-12 col-sm-7 col-lg-5 div-center">
    <?php
    echo validation_errors();
    
    //connet the form to the create account function for validation
    echo form_open('login/accountValidation');?>
        <label><strong class="uppercase"> First Name</strong></label> <br/>
    <?php echo form_input('first_name', '','placeholder="First Name" class="firstname"'); ?>
        <br/>
        <label><strong class="uppercase"> Last Name</strong></label> <br/>
    <?php echo form_input('last_name', '','placeholder="Last Name" class="lastname"'); ?>
        <br/>
        
        <label><strong class="uppercase">UNT Email Address</strong></label> <br/>
    <?php echo form_input('email', '','placeholder="UNT Email" class="password"'); ?>
        <br/>
        
        <label><strong class="uppercase">Password</strong></label> <br/>
    <?php echo form_password('password', '', 'placeholder="Password" class="password"'); ?>
        <br/>
        
        <label><strong class="uppercase">Confirm Password</strong></label> <br/>
    <?php echo form_password('password_confirm', '', 'placeholder="Password" class="password"'); ?>
        <br/>
        
        <label><strong class="uppercase">UNT ID</strong></label> <br/>
    <?php echo form_input('UNTid', '','placeholder="UNT ID" class="untid"');?>
   
            <br/> <label><strong class="uppercase">Faculty/Student</strong></label> <br/>
    <?php $options = array('faculty' => 'Faculty',
        'student' => 'Student'
    );

    echo form_dropdown('faculty_student', $options, 'student'); ?>
             
    <br/><label><strong class="uppercase">College</strong></label> <br/>
    
    <?php
    $college = array(
        'none' => 'None',
        'arts_science' => 'College of Arts and Sciences',
        'business' => 'College of Business',
        'education' => 'College of Education',
        'engineering' => 'College of Engineering',
        'information' => 'College of Information',
        'merchandising' => 'College of Merchandising, Hospitality and Tourism',
        'music' => 'College of Music',
        'community_service' => 'College of Public Affairs and Community Service',
        'visual_arts_design' => 'College of Visual Arts and Design'
    );
    echo form_dropdown('college', $college, 'none'); ?>
    <!--
    /*
    $major = array(
      'csce'   => 'Computer Science',
      'EE' => 'Electrical Engineering'
    );
     echo form_dropdown('major', $major, 'csce');
     
    $depart = array(
        'math' => 'Math',
        'phys' => 'Physics',
        'csci' => 'Computer Science'
    );        
     echo form_dropdown('department', $depart, 'csci');
     $year = array(
         '1' => 'Freshman',
                 '2' => 'Sophmore',
         '3' => 'Junior',
         '4' => 'Senior',
         '5' => 'Graduate'
    );
        echo form_dropdown('year', $year, '1'); */
        
      //use the array passed in from the models->views to populate the drop down box for available elections -->
        <br/>
        
        <label><strong class="uppercase">Major</strong></label> <br/>
       <?php echo form_input('major'); ?>
        <br/>
        
        <label><strong class="uppercase">Department</strong></label> <br/>
       <?php echo form_input('department'); ?>
        <br/>
        
        <label><strong class="uppercase">Year</strong></label> <br/>
       <?php echo form_input('year'); ?>
        <br/>
        
        <label><strong class="uppercase">Apply as Voter for</strong></label> <br/>
       <?php  echo form_multiselect('applyVoter[]', $position); ?>
        <br/>
        
        <label><strong class="uppercase">Apply as Candidate for </strong></label> <br/>
        <?php echo form_multiselect('applyCandidate[]', $position); ?>

        <br/>
    <div class="btn btn-default btn-lg">
   <?php echo form_submit('submit', 'Create Account');
    ?>
    </div>
</form>
</div>
</div>
</div>


<div id="push"></div> <!-- This pushes the footer to the bottom -->

</div>

<!-- footer starts here -->
<footer>
<p id="footer-text">Made at the University of North Texas</p>
</footer>

             
             </body>
             
             </html>