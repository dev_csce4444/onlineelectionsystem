<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ApprovalMgr extends CI_Controller {

    
    
    public function AprVoter() {
        
        
        $this->load->model('admin_model');
        
        $email=$this->input->post('email');
        
        $pos=$this->input->post('position');
        
        //echo "<script>alert('".$email." : ".$pos."');</script>";

        $this->admin_model->approve_voter(urldecode($email),urldecode($pos));
        $this->load->model('email_model');
        $this->email_model->sendMail($email,"Approved","You have been approved to Vote for".$pos);
        
        $this->loadVoterReqs();
        
    }
    public function AprCandi() {
        
        $this->load->model('admin_model');
        $email=$this->input->post('email');
        
        $pos=$this->input->post('position');
        //echo "<script>alert('".$email." : ".$pos."');</script>";

        $this->admin_model->approve_candidate($email,$pos);
        $this->load->model('email_model');
        $this->email_model->sendMail($email,"Approved","You have been approved to run for".$pos);

        $this->loadCandisReqs();
        
        
    }
    public function DisCandi() {
        
        $this->load->model('admin_model');
        $email=$this->input->post('email');
        
        $pos=$this->input->post('position');
        //echo "<script>alert('".$email." : ".$pos."');</script>";

        $this->admin_model->disapprove_candidate($email,$pos);
        $this->load->model('email_model');
        $this->email_model->sendMail($email,"Disapproved","You have not been Approved to run for".$pos);

        $this->loadCandisReqs();
        
        
    }
    public function DisVoter() {
        
        $this->load->model('admin_model');
        $email=$this->input->post('email');
        
        $pos=$this->input->post('position');
        //echo "<script>alert('".$email." : ".$pos."');</script>";

        $this->admin_model->disapprove_voter($email,$pos);
        $this->load->model('email_model');
        $this->email_model->sendMail($email,"Disapproved","You have not been Approved to Vote for".$pos);

        $this->loadVoterReqs();
        
        
        
    }
    public function DisACandi() {
        
        $this->load->model('admin_model');
        
        $email=$this->input->post('email');
        
        $pos=$this->input->post('position');
        //echo "<script>alert('".$email." : ".$pos."');</script>";

        $this->admin_model->disapprove_candidate($email,$pos);
        $this->load->model('email_model');
        $this->email_model->sendMail($email,"Disapproved","You have not been Approved to run for".$pos);

        $this->loadApprovedCandis();
       
        
    }
    public function DisAVoter() {
        
        $this->load->model('admin_model');
        $email=$this->input->post('email');
        
        $pos=$this->input->post('position');
        //echo "<script>alert('".$email." : ".$pos."');</script>";

        $this->admin_model->disapprove_voter($email,$pos);
        $this->load->model('email_model');
        $this->email_model->sendMail($email,"Disapproved","You have not been Approved to vote for".$pos);

        $this->loadApprovedVoters();
       
        
        
    }
    
    public function loadApprovedCandis() {
        
        $this->load->helper('url');
        $this->load->model('admin_model');
        $query=$this->admin_model->get_all_approved_candidates();
        $this->load->view('ApprovedCandidates',$query);
        
        
    }
    
    public function loadApprovedVoters() {
        
        $this->load->helper('url');
        $this->load->model('admin_model');
        $query=$this->admin_model->get_all_approved_voters();
        $this->load->view('ApprovedVoters',$query);
        
        
    }
    public function loadCandisReqs() {
        
        $this->load->helper('url');
        $this->load->model('admin_model');
        $query=$this->admin_model->get_all_candidate_requests();
        $this->load->view('CandiRequests',$query);
        
        
    }
    
    public function loadVoterReqs() {
        
        $this->load->helper('url');
        $this->load->model('admin_model');
        $query=$this->admin_model->get_all_voter_requests();
        $this->load->view('VoterRequests',$query);
        
        
    }
    
    public function AmgrResults() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('election_model');
        //will have to change this to get_finished_election_results once the query is fixed
        $query = $this->election_model->get_all_elections();//get_all_finished_finalized_elections();
        
        $this->load->view('AmgrVResults', $query);
        
    }
    
    public function viewAmgrResults(){
        $this->load->helper('url');
        
        $this->load->model('election_model');
        
        if(isset($_POST['position'])){
            $query = $this->election_model->view_election_votes($_POST['position']);
            $stats=$this->election_model->get_hourly_results($_POST['position']);
            //echo '<script> alert ("'.$stats.'"); </script>';
            
            $this->load->library('gcharts');
            
            $this->gcharts->load('ColumnChart');
            
            $this->gcharts->DataTable('stats')->addColumn('string', 'Interval', 'count');
            $this->gcharts->DataTable('stats')->addColumn('number', 'voteCount', 'vcount');
            for($i=0;$i<sizeof($stats);$i++){
            $this->gcharts->DataTable('stats')->addRow(array(" ".$i,
                           $stats[$i]
                           ));
            }
            
            $config = array(
                            'title' => 'stats'
                            );
            
            $this->gcharts->ColumnChart('stats')->setConfig($config);
            
            
            $this->load->view('AMgrResults', $query);
        }
    }

    
    
}

?>
