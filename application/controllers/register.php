<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class register extends CI_Controller {

    //index is the default method when the controller is called
    //public function index() {
        
        //$this->load->helper('url');
        //display the login form
        //$this->load->view('login');
        
    //}

    public function accountValidation() {
        
        
        //$this->load->view('login');
        
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules("firstName", "First Name", "trim|required|alpha_dash|min_length[2]|max_length[15]");
        $this->form_validation->set_rules("lastName", "Last Name", "trim|required|alpha_dash|min_length[2]|max_length[30]");
        $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");
        //$this->form_validation->set_rules("password_confirm", "Password Confirmation", "trim|required|matches[password]");
        $this->form_validation->set_rules("emailAddress", "Email", "trim|required|valid_email|callback_check_if_unt_email|callback_check_if_email_exists|min_length[5]|max_length[40]");
        $this->form_validation->set_rules("untID", "UNT ID", "trim|min_length[8]|max_length[8]|numeric|is_natural|callback_check_unique_id");
        //$this->form_validation->set_rules("faculty_student", "Faculty or Student", "required");
        //$this->form_validation->set_rules("major", "Major");
        //$this->form_validation->set_rules("college", "College");
        //$this->form_validation->set_rules("department", "Department");
        //$this->form_validation->set_rules("year", "Year", "required");
        //$this->form_validation->set_rules("applyVoter", "Apply to be a Voter", "required");
        //$this->form_validation->set_rules("applyCandidate", "Apply to be a Candidate", "required");
        
        if ($this->form_validation->run() == FALSE) {
            
            //echo 'wrong';
            //form validation failed
            //get all the current elections from the models
            $this->load->model('membership_model');
            
            //returns the current electsions, array indices of: $result['position'], $result['endDate']
            $query = $this->membership_model->get_current_elections_for_registration();
            $this->load->view('login', $query);
            
        } else {
            //insert into db
            $this->load->model("membership_model");
            
            //try to create the account
            //if ($query = $this->membership_model->insertUser()) {
              //  $data['account_created'] = "Your account has been created<br>";
                
                $this->load->view('register', $data);
            //}
        }
    }
    
}

?>
