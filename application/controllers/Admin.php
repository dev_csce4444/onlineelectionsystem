<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    //index is the default method when the controller is called
    public function index() {

        $this->load->helper('url');
        $this->load->model('admin_model');
        $query = $this->admin_model->get_approved_admins();
        $this->load->view('HeadAdmin', $query);
    }

    public function loadAddAdmin() {

        $this->load->helper('url');
        $this->load->view('AddAdmin');
    }

    public function AddAdmin() {

        $this->load->helper('url');
        $this->load->model('admin_model');
        $this->load->model('membership_model');
        $email = $this->input->post('emailAddress');
        $_POST['email'] = $email;
        if (!$this->membership_model->check_if_email_exists($email)) {

            if ($_POST['group1'] == 'HeadAdmin') {
                $_POST['head_admin'] = 1;
                $_POST['monitor_election'] = 0;
                $_POST['add_candidate'] = 0;
                $_POST['create_election'] = 0;
                $_POST['remove_election'] = 0;
                $_POST['remove_candidate'] = 0;
                $_POST['approve_registration'] = 0;
            } else if ($_POST['group1'] == 'ElectionManager') {
                $_POST['head_admin'] = 0;
                $_POST['monitor_election'] = 0;
                $_POST['add_candidate'] = 0;
                $_POST['create_election'] = 1;
                $_POST['remove_election'] = 0;
                $_POST['remove_candidate'] = 0;
                $_POST['approve_registration'] = 0;
            } else if ($_POST['group1'] == 'ElectionMonitor') {
                $_POST['head_admin'] = 0;
                $_POST['monitor_election'] = 1;
                $_POST['add_candidate'] = 0;
                $_POST['create_election'] = 0;
                $_POST['remove_election'] = 0;
                $_POST['remove_candidate'] = 0;
                $_POST['approve_registration'] = 0;
            } else if ($_POST['group1'] == 'ApprovalManager') {
                $_POST['head_admin'] = 0;
                $_POST['monitor_election'] = 0;
                $_POST['add_candidate'] = 0;
                $_POST['create_election'] = 0;
                $_POST['remove_election'] = 0;
                $_POST['remove_candidate'] = 0;
                $_POST['approve_registration'] = 1;
            }
            if ($this->admin_model->insert_admin()) {
                $query = $this->admin_model->get_approved_admins();
                $this->load->view('HeadAdmin', $query);
            } else {
                $this->load->helper('url');
                $this->load->view('AddAdmin');
                echo "<script>alert('Could not insert admin . Please try again');</script>";
            }
        } else {
            $this->load->helper('url');
            $this->load->view('AddAdmin');
            echo "<script>alert('Could not insert admin . User needs to be registered with the system inorder to be added as an admin');</script>";
        }
    }

    public function Edit($email) {

        $this->load->helper('url');
        $this->load->model('admin_model');
        $query = $this->admin_model->get_admin_permissions(urldecode($email));
        $this->load->view('EditAdmin', $query);
    }

    public function Delete($email) {

        $this->load->helper('url');
        $this->load->model('admin_model');
        $flag = $this->admin_model->delete_admin(urldecode($email));
        $query = $this->admin_model->get_approved_admins();
        $this->load->view('HeadAdmin', $query);
    }

    public function EditAdmin() {

        $this->load->helper('url');
        $this->load->model('admin_model');
        $email = $this->input->post('emailAddress');
        //$_POST['email']=$email;
        if ($_POST['group1'] == 'HeadAdmin') {
            $_POST['head_admin'] = 1;
            $_POST['monitor_election'] = 0;
            $_POST['add_candidate'] = 0;
            $_POST['create_election'] = 0;
            $_POST['remove_election'] = 0;
            $_POST['remove_candidate'] = 0;
            $_POST['approve_registration'] = 0;
        } else if ($_POST['group1'] == 'ElectionManager') {
            $_POST['head_admin'] = 0;
            $_POST['monitor_election'] = 0;
            $_POST['add_candidate'] = 0;
            $_POST['create_election'] = 1;
            $_POST['remove_election'] = 0;
            $_POST['remove_candidate'] = 0;
            $_POST['approve_registration'] = 0;
        } else if ($_POST['group1'] == 'ElectionMonitor') {
            $_POST['head_admin'] = 0;
            $_POST['monitor_election'] = 1;
            $_POST['add_candidate'] = 0;
            $_POST['create_election'] = 0;
            $_POST['remove_election'] = 0;
            $_POST['remove_candidate'] = 0;
            $_POST['approve_registration'] = 0;
        } else if ($_POST['group1'] == 'ApprovalManager') {
            $_POST['head_admin'] = 0;
            $_POST['monitor_election'] = 0;
            $_POST['add_candidate'] = 0;
            $_POST['create_election'] = 0;
            $_POST['remove_election'] = 0;
            $_POST['remove_candidate'] = 0;
            $_POST['approve_registration'] = 1;
        }
        if ($this->admin_model->modify_admin_permissions()) {
            $query = $this->admin_model->get_approved_admins();
            $this->load->view('HeadAdmin', $query);
        } else {
            $this->load->view('login');
            echo $email;
            echo $_POST['group1'];
        }
    }

    public function HAElectionResults() {

        $this->load->helper('url');
        $this->load->model('election_model');
        $query = $this->election_model->get_all_elections(); 
        $this->load->view('AdminViewResults', $query);
    }

    public function loadHAProfilesBrowse() {

        $this->load->helper('url');
        $this->load->model('candidate_model');
        $query = $this->candidate_model->get_all_candidates();
        $this->load->view('AdminBrowseProfiles', $query);
    }

}

?>
