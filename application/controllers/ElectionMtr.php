<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ElectionMtr extends CI_Controller {

    
    
    public function giveFeed($pos) {
        
        $this->load->model('admin_model');
        $_POST['election_name']= urldecode($pos);
        $this->load->model('election_model');
        if($this->admin_model->give_feedback())
        {
            $query=$this->election_model->get_all_finished_elections();
            $this->load->view('MtrCompElections',$query);
        }
        else
        {
            echo "<script>alert('Could not give feedback please try again');</script>";
        }
    }
    
    public function loadElectionFeedback($pos) {
        
        $this->load->model('admin_model');
        $_POST['position']= urldecode($pos);
        $query=$this->admin_model->get_election_all_votes();
        $this->load->view('ElectionFeedBack',$query);
        
        
    }
    public function loadMtrCompElections() {
        
        $this->load->helper('url');
        $this->load->model('election_model');
        $query=$this->election_model->get_all_finished_elections();
        $this->load->view('MtrCompElections',$query);
        
        
    }
    
    public function loadMtrCurElections() {
        
        $this->load->helper('url');
        $this->load->model('election_model');
        $query=$this->election_model->get_all_elections();
        $this->load->view('MtrCurElections',$query);
        
        
    }
    
    public function EMtrResults() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('election_model');
        //will have to change this to get_finished_election_results once the query is fixed
        $query = $this->election_model->get_all_elections();//get_all_finished_finalized_elections();
        
        $this->load->view('EMtrVResults', $query);
        
    }
    
    public function viewEmtrResults(){
        $this->load->helper('url');
        
        $this->load->model('election_model');
        
        if(isset($_POST['position'])){
            $query = $this->election_model->view_election_votes($_POST['position']);
            $stats=$this->election_model->get_hourly_results($_POST['position']);
            
            $this->load->library('gcharts');
            
            $this->gcharts->load('ColumnChart');
            
            $this->gcharts->DataTable('stats')->addColumn('string', 'Interval', 'count');
            $this->gcharts->DataTable('stats')->addColumn('number', 'voteCount', 'vcount');
            for($i=0;$i<sizeof($stats);$i++){
                $this->gcharts->DataTable('stats')->addRow(array(" ".$i,
                                                                 $stats[$i]
                                                                 ));
            }
            
            $config = array(
                            'title' => 'stats'
                            );
            
            $this->gcharts->ColumnChart('stats')->setConfig($config);
            
            
            $this->load->view('EMtrResults', $query);
        }
    }

    

    
}

?>
