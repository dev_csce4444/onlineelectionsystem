<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    //index is the default method when the controller is called
    public function index() {
        $this->load->helper('url');
        //display the login form
        $this->load->view('login');
        
    }

    //the controller that handles the signup page
    //called from login_form view    
    public function signup() {

        //get all the current elections from the models
        $this->load->model('membership_model');
        
        //returns the current electsions, array indices of: $result['position'], $result['endDate']
        $query = $this->membership_model->get_current_elections_for_registration();
        
        
        //pass the current elections into the views to show
        $this->load->view('create_account', $query);
    }

    //controller to handle the validation for the login information
    //called from login_form view
    public function validate_login() {
        
        
        $this->load->model('membership_model');
        $query1 = $this->membership_model->login();
        
        //store sessions info and custom user data
        //storing the email in the session enables you to pull it out whenever you need
        if ($query1) { //if the user info is good
            
           $email=$this->session->userdata('email');
            
            $this->load->model('candidate_model');
            $this->load->model('admin_model');
            if($this->admin_model->is_user_head_admin($email))
            {
                $this->load->helper('url');
                $query2 = $this->admin_model->get_approved_admins();
                $this->load->view('HeadAdmin',$query2);
                
            }
            else if($this->admin_model->is_user_an_admin($email))
            {
                $result=$this->admin_model->get_admin_permissions($email);
                if($result['approve_registration'][0]==1)
                {
                    $this->load->helper('url');
                    $this->load->model('admin_model');
                    $query3=$this->admin_model->get_all_voter_requests();
                    $this->load->view('VoterRequests',$query3);
                }
                else if($result['create_election'][0]==1)
                {
                    $this->load->helper('url');
                    $this->load->model('election_model');
                    $query4=$this->election_model->get_all_elections();
                    $this->load->view('ElectionManager',$query4);
                    
                }
                else
                {
                    $this->load->helper('url');
                    $this->load->model('election_model');
                    $query5=$this->election_model->get_all_elections();
                    $this->load->view('MtrCurElections',$query5);
                    
                    
                }
                
            }
            else if($this->candidate_model->is_user_a_candidate($email))
            {
                  $this->load->model('election_model');
                      $query = $this->election_model->get_current_eligible_elections($this->session->userdata('email'));
        $this->load->view('CandiElections', $query);  
                
            
            }
            else
            {
        $this->load->model('election_model');
                $query = $this->election_model->get_current_eligible_elections($this->session->userdata('email'));
        $this->load->view('VoterElections', $query);  
        
             
            }
            
            
        } else { //no valid info

            $this->load->helper('url');
            $this->load->view('login');
            echo '<script>alert("Invalid Credentials");</script>';
            
        }
    }

    //controller to create a new account
    //handles the validation for the information entered from the creat_account view page
    public function accountValidation() {
        
        $this->load->library('form_validation');
        $this->load->model('email_model');
        
        $this->form_validation->set_rules("first_name", "First Name", "trim|required|alpha_dash|min_length[2]|max_length[15]");
        $this->form_validation->set_rules("last_name", "Last Name", "trim|required|alpha_dash|min_length[2]|max_length[30]");
        $this->form_validation->set_rules("password", "Password", "trim|required|min_length[6]|max_length[32]");
        $this->form_validation->set_rules("password_confirm", "Password Confirmation", "trim|required|matches[password]");
        $this->form_validation->set_rules("email", "Email", "trim|required|valid_email|callback_check_if_unt_email|callback_check_if_email_exists|min_length[5]|max_length[40]");
        $this->form_validation->set_rules("UNTid", "UNT ID", "trim|min_length[8]|max_length[8]|numeric|is_natural|callback_check_unique_id");
        $this->form_validation->set_rules("faculty_student", "Faculty or Student", "required");
        $this->form_validation->set_rules("major", "Major");
        $this->form_validation->set_rules("college", "College");
        $this->form_validation->set_rules("department", "Department");
        $this->form_validation->set_rules("year", "Year", "required");
        $this->form_validation->set_rules("applyVoter", "Apply to be a Voter", "required");
        $this->form_validation->set_rules("applyCandidate", "Apply to be a Candidate", "required");
        
        if ($this->form_validation->run() == FALSE) {
            //form validation failed
            //get all the current elections from the models
            $this->load->model('membership_model');
            $query = $this->membership_model->get_current_elections_for_registration();
            $this->load->view('create_account', $query);
            
        } else {
            
            if(!$this->email_model->sendMail($this->input->post('email'),"No Reply", "To verify your email by UNT Voter System"))
            {
                
                //returns the current electsions, array indices of: $result['position'], $result['endDate']
                $query = $this->membership_model->get_current_elections_for_registration();
                $this->load->view('create_account', $query);
                echo "<script>alert('Sorry unable to verify your email address');</script>";
                
            }
            else{
                
            //insert into db
            $this->load->model("membership_model");
            
            
            
            //try to create the account
            if ($query = $this->membership_model->insertUser()) {
                $data['account_created'] = "Your account has been created<br>";
                
                $this->load->view('login', $data);
                echo "<script>alert('Account Created');</script>";
            }
                
            }
        }
    }

    //check to make sure the id has not been entered already
    public function check_unique_id($request_UNTid) {
        $this->load->model('membership_model');
        $unique = $this->membership_model->check_unique_id($request_UNTid);
        if ($unique)
            return TRUE;
        else
            return FALSE;
    }

    //custom call back function for form validation
    public function check_if_unt_email($requested_email) {
        if (stripos($requested_email, "@unt.edu") === FALSE && stripos($requested_email, "@my.unt.edu") === FALSE)
            return FALSE;
        else
            return TRUE;
    }

    //custom call back function for form validation
    public function check_if_email_exists($requested_email) {
        $this->load->model('membership_model'); //prepare the sql statement        
        $available = $this->membership_model->check_if_email_exists($requested_email);
        if ($available)
            return TRUE;
        else
            return FALSE;
    }
    
    public function loadfpswd(){
    
        $this->load->view('fPswd');
        
    }
    
    public function fpswd()
    {
        $email = $this->input->post('email');
        $this->load->model('email_model');
        $this->load->model('membership_model');
        if(!$this->membership_model->check_if_email_exists($email))
        {
            $query1=$this->membership_model->password_recovery($email);
            $message= "The credentials u requested are Email : ".$query1['email']." Password: ".$query1['password'];
            if($this->email_model->sendMail($email,"Credentials for UNT Online Election System",$message))
            {
                echo "<script>alert('Credentials have been emailed to you');</script>";
            }
            else
            {
                echo "<script>alert('No such registered user');</script>";
            }
            
            $this->load->view('login');
            //echo "<script>alert('Email sent to user');</script>";
            
            
        }
        else
        {
            $this->load->view('fPswd');
            echo "<script>alert('No such registered user');</script>";
        }
        
        
        
    }
    
    public function logout()
    {
        $this->load->model('membership_model');
        $this->membership_model->logout($this->session->userdata('email'));
        $this->load->view('login');
        
    }
    



}

?>
