<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Unittest extends CI_Controller {

    //index is the default method when the controller is called
    public function index() {

        //load the unit test library
        $this->load->library('unit_test');

        //test the admin model functions
        $this->load->model('admin_model');

        $test_name = 'admin_model: get_all_admins';
        $this->unit->run($this->admin_model->get_all_admins(), 'is_array', $test_name);
        
        $test_name = 'admin_model: get_approved_admins';
        $this->unit->run($this->admin_model->get_approved_admins(), 'is_array', $test_name);
  
        $test_name = 'admin_model: is_user_an_admin in table';
        $this->unit->run($this->admin_model->is_user_an_admin('donaldmartin@my.unt.edu'), TRUE, $test_name);
 
        $test_name = 'admin_model: is_user_an_admin not in table';
        $this->unit->run($this->admin_model->is_user_an_admin('donin@my.unt.edu'), FALSE, $test_name);

        $test_name = 'admin_model: is_user_head_admin in table';
        $this->unit->run($this->admin_model->is_user_head_admin('donaldmartin@my.unt.edu'), TRUE, $test_name);
        
        $test_name = 'admin_model: is_user_head_admin not in table';
        $this->unit->run($this->admin_model->is_user_head_admin('donrtin@my.unt.edu'), FALSE, $test_name);
        
        $test_name = 'admin_model: insert_admin doesnt exists';
        $this->unit->run($this->admin_model->insert_admin('noadmin@my.unt.edu'), TRUE, $test_name);
        
        $test_name = 'admin_model: insert_admin already exists';
        $this->unit->run($this->admin_model->insert_admin('donaldmartin@my.unt.edu'), FALSE, $test_name);
        
        $test_name = 'admin_model: delete_admin  in table';
        $this->unit->run($this->admin_model->delete_admin('noadmin@my.unt.edu'), TRUE, $test_name);
        
        $test_name = 'admin_model: delete_admin not in table';
        $this->unit->run($this->admin_model->delete_admin('BILLYbob@my.unt.edu'), FALSE, $test_name);
        
        $test_name = 'admin_model: get_admin_permissions';
        $this->unit->run($this->admin_model->get_admin_permissions('donaldmartin@my.unt.edu'), 'is_array', $test_name);

        $test_name = 'admin_model: get_all_admin_permissions';
        $this->unit->run($this->admin_model->get_all_admin_permissions(), 'is_array', $test_name);

        $test_name = 'admin_model: modify_admin_permissions in table';
        $_POST['monitor_election'] = 1;
        $this->unit->run($this->admin_model->modify_admin_permissions('asdf@unt.edu'), TRUE, $test_name);
        $_POST['monitor_election'] = 0;
        $this->admin_model->modify_admin_permissions('asdf@unt.edu');
        
        $test_name = 'admin_model: modify_admin_permissions not in table';
        $this->unit->run($this->admin_model->modify_admin_permissions('BILLYbob@my.unt.edu'), FALSE, $test_name);

        $test_name = 'admin_model: create_new_election in table';
        $this->unit->run($this->admin_model->create_new_election('UNIT TEST'), TRUE, $test_name);

        $test_name = 'admin_model: create_new_election not in table';
        $this->unit->run($this->admin_model->create_new_election('UNIT TESTadsf'), FALSE, $test_name);

   
        $test_name = 'admin_model: modify_election in table';
        $_POST['description'] = "alsdfkjtyujkasdf";
        $this->unit->run($this->admin_model->modify_election('UNIT TEST'), TRUE, $test_name);
$_POST['description'] = "al";
   $this->admin_model->modify_election('UNIT TEST');
   
   $test_name = 'admin_model: modify_election not in table';
        $this->unit->run($this->admin_model->modify_election('UNIT TESTasdf'), FALSE, $test_name);                

      
   
   
        $test_name = 'admin_model: get_election_information_and_feedback_for_winner';
        $this->unit->run($this->admin_model->get_election_information_and_feedback_for_winner('president'), 'is_array', $test_name);

   
        $test_name = 'admin_model: get_election_all_votes';
        $this->unit->run($this->admin_model->get_election_all_votes('UNIT TEST'), 'is_array', $test_name);


        $test_name = 'admin_model: view_election_votes';
        $this->unit->run($this->admin_model->view_election_votes('UNIT TEST'), 'is_array', $test_name);

        $test_name = 'admin_model: finalize_election_results in table';
        $this->unit->run($this->admin_model->finalize_election_results('UNIT TEST'), TRUE, $test_name);

        $test_name = 'admin_model: finalize_election_results not in table';
        $this->unit->run($this->admin_model->finalize_election_results('UNIT TESTaspodf'), FALSE, $test_name);

           $test_name = 'admin_model: give_feedback in table';
           $_POST['feedback'] = "lkj";
        $this->unit->run($this->admin_model->give_feedback('UNIT TEST'), TRUE, $test_name);
$_POST['feedback'] = "l";
$this->admin_model->give_feedback('UNIT TEST');

        $test_name = 'admin_model: give_feedback not in table';
        $this->unit->run($this->admin_model->give_feedback('UNIT TEST asdrf'), FALSE, $test_name);

        
        $test_name = 'admin_model: remove_election in table';
        $this->unit->run($this->admin_model->remove_election('UNIT TEST'), FALSE, $test_name);

        $test_name = 'admin_model: remove_election not in table';
        $this->unit->run($this->admin_model->remove_election('UNIT TESTqwadf'), FALSE, $test_name);

     
        $test_name = 'admin_model: get_re_election_requests';
        $this->unit->run($this->admin_model->get_re_election_requests(), 'is_array', $test_name);

        $test_name = 'admin_model: get_all_approved_voters';
        $this->unit->run($this->admin_model->get_all_approved_voters(), 'is_array', $test_name);

        $test_name = 'admin_model: get_all_approved_candidates';
        $this->unit->run($this->admin_model->get_all_approved_candidates(), 'is_array', $test_name);

        $test_name = 'admin_model: get_all_voter_requests';
        $this->unit->run($this->admin_model->get_all_voter_requests(), 'is_array', $test_name);

        $test_name = 'admin_model: get_all_candidate_requests';
        $this->unit->run($this->admin_model->get_all_candidate_requests(), 'is_array', $test_name);

        $test_name = 'admin_model: get_all_approved_elections_for_voter';
        $this->unit->run($this->admin_model->get_all_approved_elections_for_voter('donaldmartin@my.unt.edu'), 'is_array', $test_name);



        $test_name = 'admin_model: approve_candidate';
        $this->unit->run($this->admin_model->approve_candidate('dasdfs@unt.edu', 'president'), FALSE, $test_name);



        $test_name = 'admin_model: approve_voter';
        $this->unit->run($this->admin_model->approve_voter('don@my.unt.edu', 'president'), FALSE, $test_name);

   

        $test_name = 'admin_model: disapprove_candidate';
        $this->unit->run($this->admin_model->disapprove_candidate('A@UNT.edu', 'president'), FALSE, $test_name);


        $test_name = 'admin_model: disapprove_voter ';
        $this->unit->run($this->admin_model->disapprove_voter('A@unt.edu', 'president'), FALSE, $test_name);

        $test_name = 'admin_model: get_all_users';
        $this->unit->run($this->admin_model->get_all_users(), 'is_array', $test_name);

        $test_name = 'admin_model: get_all_candidates';
        $this->unit->run($this->admin_model->get_all_candidates(), 'is_array', $test_name);

        $test_name = 'admin_model: get_all_voters';
        $this->unit->run($this->admin_model->get_all_voters(), 'is_array', $test_name);

        $test_name = 'admin_model: check_if_admin_email_exists in table';
        $this->unit->run($this->admin_model->check_if_admin_email_exists('donaldmartin@my.unt.edu'), FALSE, $test_name);

        $test_name = 'admin_model: check_if_admin_email_exists not in table';
        $this->unit->run($this->admin_model->check_if_admin_email_exists('sd@my.unt.edu'), TRUE, $test_name);

     

        //election model unit tests          
        $this->load->model('election_model');


        $test_name = 'election_model: has_user_voted has voted';
        $this->unit->run($this->election_model->has_user_voted('asdf@unt.edu', 'president'), TRUE, $test_name);

        $test_name = 'election_model: has_user_voted  hasnt voted';
        $this->unit->run($this->election_model->has_user_voted('asdf@unt.edu', 'vice president'), FALSE, $test_name);

        $test_name = 'election_model: get_current_eligible_elections ';
        $this->unit->run($this->election_model->get_current_eligible_elections(), 'is_array', $test_name);

        $test_name = 'election_model: get_candidates_for_electio';
        $this->unit->run($this->election_model->get_candidates_for_election('president'), 'is_array', $test_name);



        $test_name = 'election_model: vote_for_candidate ';
        $this->unit->run($this->election_model->vote_for_candidate('donaldmartin@my.unt.edu', 'president', 'd@unt.edu'), FALSE, $test_name);



        $test_name = 'election_model: write_in_candidate ';
        $this->unit->run($this->election_model->write_in_candidate('dodmartin@my.unt.edu', 'president', 'd@unt.edu'), FALSE, $test_name);

        $test_name = 'election_model: view_election_votes not in table';
        $this->unit->run($this->election_model->view_election_votes('president'), 'is_array', $test_name);

        $test_name = 'election_model: finalize_election_results in table';
        $this->unit->run($this->election_model->finalize_election_results('president'), FALSE, $test_name);

        $test_name = 'election_model: finalize_election_results not in table';
        $this->unit->run($this->election_model->finalize_election_results('jhgfd'), FALSE, $test_name);

        $test_name = 'election_model: get_finished_election_results ';
        $this->unit->run($this->election_model->get_finished_election_results('president'), 'is_array', $test_name);

        $test_name = 'election_model: create_new_election doesnt exist';
        $this->unit->run($this->election_model->create_new_election('treasurry'), TRUE, $test_name);

        $test_name = 'election_model: create_new_election exists';
        $this->unit->run($this->election_model->create_new_election('president'), FALSE, $test_name);



        $test_name = 'election_model: reelection_request';
        $this->unit->run($this->election_model->reelection_request('LKJHGFD'), FALSE, $test_name);

    

        $test_name = 'election_model: remove_election ';
        $this->unit->run($this->election_model->remove_election('yuio'), FALSE, $test_name);
           
    
        //unit tests for candidate model
         $this->load->model('candidate_model');
         
         $test_name = 'candidate_model: get_candidate_info  ';
        $this->unit->run($this->candidate_model->get_candidate_info('donaldmartin@my.unt.edu'), 'is_array', $test_name);
        
         $test_name = 'candidate_model: is_user_a_candidate  ';
        $this->unit->run($this->candidate_model->is_user_a_candidate('donaldmartin@my.unt.edu'), TRUE, $test_name);
        
         $test_name = 'candidate_model: is_user_a_candidate  ';
        $this->unit->run($this->candidate_model->is_user_a_candidate('oASDp@unt.edu'), FALSE, $test_name);
        
         $test_name = 'candidate_model: view_candidate_info  ';
        $this->unit->run($this->candidate_model->view_candidate_info('donaldmartin@my.unt.edu'), 'is_array', $test_name);
        
         $test_name = 'candidate_model: update_bio_held_positions in table ';
         $_POST['bio'] = "hgfd";
        $this->unit->run($this->candidate_model->update_bio_held_positions('donaldmartin@my.unt.edu'), TRUE, $test_name);
        $_POST['bio'] = "h";
        $this->candidate_model->update_bio_held_positions('donaldmartin@my.unt.edu');
        
         $test_name = 'candidate_model: update_bio_held_positions  not in table';
        $this->unit->run($this->candidate_model->update_bio_held_positions('oRTFGp@unt.edu'), FALSE, $test_name);
        
         $test_name = 'candidate_model: get_all_candidates  ';
        $this->unit->run($this->candidate_model->get_all_candidates(), 'is_array', $test_name);
             
   
             
        //unit tests for membership model
         $this->load->model('membership_model');
     
        
           $test_name = 'membership_model: apply_to_be_voter ';
        $this->unit->run($this->membership_model->apply_to_be_voter('donaldmartin@my.unt.edu','president'), FALSE, $test_name);
        
           $test_name = 'membership_model: apply_to_be_candidate  ';
        $this->unit->run($this->membership_model->apply_to_be_candidate('l@unt.edu','president'), FALSE, $test_name);
        
        
           $test_name = 'membership_model: check_if_email_exists  it does';
        $this->unit->run($this->membership_model->check_if_email_exists('donaldmartin@my.unt.edu'), FALSE, $test_name);
        
           $test_name = 'membership_model: check_if_email_exists  it doesnt';
        $this->unit->run($this->membership_model->check_if_email_exists('lkjhgf@unt.edu'), TRUE, $test_name);
        
           $test_name = 'membership_model: check_unique_id  it does';
        $this->unit->run($this->membership_model->check_unique_id('10481065'), FALSE, $test_name);
        
           $test_name = 'membership_model: check_unique_id  it doesnt';
        $this->unit->run($this->membership_model->check_unique_id('3'), TRUE, $test_name);
        
           $test_name = 'membership_model: get_current_elections_for_registration';
        $this->unit->run($this->membership_model->get_current_elections_for_registration(), 'is_array', $test_name);
        
         $test_name = 'membership_model: password_recovery';
        $this->unit->run($this->membership_model->password_recovery('donaldmartin@my.unt.edu'), 'is_array', $test_name);
        
            echo $this->unit->report();
    }

}

?>
