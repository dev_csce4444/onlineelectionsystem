<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Voter extends CI_Controller {

    //index is the default method when the controller is called
    //loads the voterelections page 
    //goes through the database and selects all elections
    public function index() {
        
        $this->load->helper('url');
        //display the login form
        $this->load->model('election_model');
        $query = $this->election_model->get_current_eligible_elections($this->session->userdata('email'));
        $this->load->view('VoterElections', $query);   
    }


    //the controller that handles the profiles of candidates  
    public function voterProfiles() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('candidate_model');
        $query = $this->candidate_model->get_all_candidates();
        $this->load->view('profilesVoter', $query);

    }



    //the controller that handles the profiles of candidates  
    public function voterProfilesMore() {
        $this->load->helper('url');
        //display the login form

            if (isset($_POST['last_name'])) 
            {
                
                $email =  $_POST['email'];
                
            }

                $this->load->model('candidate_model');
                    $query = $this->candidate_model->get_candidate_info($email[0]);

                $this->load->view('ViewProfile', $query);

    }

    //the controller that handles voter election results page   
    public function voterElectionResults() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('election_model');
        //will have to change this to get_finished_election_results once the query is fixed
        $query = $this->election_model->get_all_elections();//get_all_finished_finalized_elections();
        
        $this->load->view('VoterElectionResults', $query);

    }

    public function viewVoterResults(){
           $this->load->helper('url');
        
        $this->load->model('election_model');
        
        if(isset($_POST['position'])){
         $query = $this->election_model->view_election_votes($_POST['position']);
            $stats=$this->election_model->get_hourly_results($_POST['position']);
            
            $this->load->library('gcharts');
            
            $this->gcharts->load('ColumnChart');
            
            $this->gcharts->DataTable('stats')->addColumn('string', 'Interval', 'count');
            $this->gcharts->DataTable('stats')->addColumn('number', 'voteCount', 'vcount');
            for($i=0;$i<sizeof($stats);$i++){
                $this->gcharts->DataTable('stats')->addRow(array(" ".$i,
                                                                 $stats[$i]
                                                                 ));
            }
            

            $config = array(
                            'title' => 'stats'
                            );
            
            $this->gcharts->ColumnChart('stats')->setConfig($config);
            

         $this->load->view('VoterViewResults', $query);
        }
    }

    //the controller that handles voter vote page   
    public function voterVote() {
        $this->load->helper('url');
        //display the login form

        
            if (isset($_POST['position'])) { 
            $position =  $_POST['position'];

            $this->load->model('election_model');
            $query = $this->election_model->get_candidates_for_election($position);
                
            $this->load->view('VoterVote', $query);
            }

            else 
            {
                 echo "Please submit the form."; 
            }
    }


    //the controller that sumbit 
    public function submitVote() {
        $this->load->helper('url');
        //display the login form
        
            if (isset($_POST['email'])) 
            {
            $candemail =  $_POST['email'];
                if ($candemail=="")
                {
                    //echo "writein";
                    //$this->load->model('election_model');
                    //$query = $this->election_model->get_candidates_for_election($position);
                     $position =  $_POST['position'][0];

                    $this->load->model('election_model');
                    $query = $this->election_model->get_candidates_for_election($position);
            
                    $this->load->view('VoterWritein', $query);   
                    }
            

                else
                    {   
                    $position =  $_POST['position'][0];
                    $voterEmail = $this->session->userdata('email');
                    $this->load->model('election_model');
            
                    //echo " it's a ",$position;
                    //echo "email is",$voterEmail;
                    $query = $this->election_model->vote_for_candidate($voterEmail,$position,$candemail);
                    $receipt = $this->election_model->get_voter_id($voterEmail,$position);
                    
                    $this->load->model('email_model');
                        $this->email_model->sendMail($voterEmail,'Vote Confirmation number and receipt','Dear Voter, \n Yout vote confirmation mumber is :'.$receipt['id'].'for the election:'.$position.'\n Thank you for voting.');
                    //post to twitter
                    //$this->load->model('twitter_model');
                    //$this->twitter_model->publish_tweets($voterEmail, $position);

                    $this->load->view('Votersuccesspage', $receipt);
                    //echo "good";
                    }
            }
            else 
            {
                 echo "Please submit the form."; 
            }
    }

        
        //the controller that that write in votes 
    public function Votesuccesswritein() {
        $this->load->helper('url');
        //display the login form
        
            if (isset($_POST['email'])) 
            {
            $candemail =  $_POST['email'];
            $position =  $_POST['position'][0];
            $voterEmail = $this->session->userdata('email');
            $voterEmail = $this->session->userdata('email');
            

            $this->load->model('election_model');
            $query = $this->election_model->write_in_candidate($voterEmail,$position,$candemail);

            //post to twitter
            //$tweet = $position;
            //$this->load->model('twitter_model');
            //$this->twitter_model->publish_tweets($voterEmail, $position);
                $receipt = $this->election_model->get_voter_id($voterEmail,$position);
                
                $this->load->model('email_model');
                $this->email_model->sendMail($voterEmail,'Vote Confirmation number and receipt','Dear Voter, \n Yout vote confirmation mumber is :'.$receipt['id'].'for the election:'.$position.'\n Thank you for voting.');
                
                
            $this->load->view('Votersuccesspage', $receipt);
            }
            else 
            {
                 echo "Please submit THE form."; 
            }
    }





    //the controller that handles the load update profile    
    public function VoterCreateProfile() {
        $this->load->helper('url');
    
        $this->load->view('VoterCreateProfile');
    }


    //the controller that handles update profile   
    public function VoterCreateProfilePost() {
        $this->load->helper('url');
        //display the login form
       
        
            if (isset($_POST['oldPassword']) && isset($_POST['newPassword'])&& isset($_POST['cNewPassword'])) 
            {
                 if($_POST['newPassword'] === $_POST['cNewPassword']){
                
                    $email = $this->session->userdata('email');
                     
                    $this->load->model('membership_model');
                    $query = $this->membership_model->change_password($email,$_POST['oldPassword'],$_POST['newPassword']);
                     if($query){
                         
                     echo '<script>alert("Success");</script>';
                     $this->load->view('Votersuccess');
                         
                         
                         
                     }
               
                     
            else echo "Please make sure you have entered the correct old password.";
                 }
                 else {
                      echo "Please make sure your new password and your password confirmation are the same.";
                 }
            }
            else 
            {
                 echo "Please submit the form."; 
            }
    }

        //the controller that handles apply voter/cand   
        public function Voterapplyvoter() {
        $this->load->helper('url');
        //display the login form
        $voterEmail = $this->session->userdata('email');
        $this->load->model('membership_model');
        $query = $this->membership_model->get_current_elections_for_registration();
        $this->load->view('Voterapplyvoter',$query);
    }


            //the controller that handles apply voter/cand   
        public function Voterapplyvotersumbit() 
        {
            $this->load->helper('url');
            //display the login form
            if (isset($_POST['election'])) 
            {
                  if($_POST['election'] ==='None'){
                    echo '<script>alert("Success");</script>';
                      $this->load->view('Votersuccess');
                }
                else
                {
                    $position = $_POST['election'];
                    $email = $this->session->userdata('email');
                    $this->load->model('membership_model');
                    $query = $this->membership_model->apply_to_be_voter($email,$position);
                    echo '<script>alert("Success");</script>';
                    $this->load->view('Votersuccess');
                }
            }
            else 
            {
                 echo "Please submit the form."; 
            }

        }



                //the controller that handles apply voter/cand   
        public function Voterapplycandi() {
        $this->load->helper('url');
        //display the login form
        $voterEmail = $this->session->userdata('email');
        $this->load->model('membership_model');
        $query = $this->membership_model->get_current_elections_for_registration();
        $this->load->view('Voterapplycandi',$query);
    }


            //the controller that handles apply voter/cand   
        public function Voterapplycandisumbit() 
        {
            $this->load->helper('url');
            //display the login form
            if (isset($_POST['election'])) 
            {
                if($_POST['election'] ==='None'){
                    echo '<script>alert("Success");</script>';
                    $this->load->view('Votersuccess');
                }
                else
                {
                    $position = $_POST['election'];
                    $email = $this->session->userdata('email');
                    $this->load->model('membership_model');
                    $query = $this->membership_model->apply_to_be_candidate($email,$position);
                    echo '<script>alert("Success");</script>';
                    $this->load->view('Votersuccess');
                }
            }
            else 
            {
                 echo "Please submit the form."; 
            }

        }
}

?>






