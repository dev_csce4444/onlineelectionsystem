<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ElectionMgr extends CI_Controller {

    
    
    public function loadAddElection() {
        
        $this->load->view('AddElection');

    }
    
    public function loadCurrentElections(){
        $this->load->model('election_model');
           $query=$this->election_model->get_all_current_elections();
                
          $this->load->view('CurrentElections',$query);
           
    }
    
    public function AddElection() {
    
        $this->load->model('election_model');
        $this->load->model('admin_model');
        $rstartDate= $this->input->post('registration_start');
        $rendDate= $this->input->post('registration_over');
        $estartDate= $this->input->post('election_start');
        $eendDate= $this->input->post('election_over');
        $dateFormat='/^(19|20)\d\d[\-\/.](0[1-9]|1[012])[\-\/.](0[1-9]|[12][0-9]|3[01])$/';
        if(!preg_match($dateFormat, $rstartDate)||!preg_match($dateFormat, $rendDate)||!preg_match($dateFormat, $estartDate)||!preg_match($dateFormat,$eendDate))
        {
            $this->load->view('AddElection');
            echo "<script>alert('Unable to add please check your date formats ');</script>";
        
        }
        else
        {
            /*$rstartDate= explode("-",$rstartDate);
            $rendDate= explode("-",$rendDate);
            $estartDate= explode("-",$estartDate);
            $eendDate= explode("-",$eendDate);
            
            if(checkdate($rstartDate[1], $rstartDate[2], $rstartDate[0])&&checkdate($rstartDate[1], $rstartDate[2], $rstartDate[0])&&checkdate($rendDate[1], $rendDate[2], $rendDate[0])&& checkdate($estartDate[1], $estartDate[2], $estartDate[0])&& checkdate($eendDate[1], $eendDate[2], $eendDate[0])){*/
            if($this->admin_model->create_new_election())
            {
                $query=$this->election_model->get_all_up_coming_elections();
                $this->load->model('email_model');
                $mails= $this->admin_model->get_all_users();         
             
                   for($i=0;$i<count($mails['email']);$i++){            
                $this->email_model->sendMail($mails['email'][$i],"Election Added", "A new election has been added to the UNT election system" );
        }
   
        
                $this->load->view('ElectionManager',$query);
                
                
                
            }
            else{
            
            
                $this->load->view('AddElection');
                echo "<script>alert('Unable to add please check your date ranges ');</script>";
        
            }
           /* }
            else
            {
                $this->load->view('AddElection',$query);
                echo "<script>alert('Unable to add please check your date formats ');</script>";
            
            }*/
        }
        
        
        
     
    }
    public function loadEditElection($pos) {
        
        $this->load->model('election_model');
        $this->load->model('admin_model');
        
        $_POST['position']= urldecode($pos);
        
        $query=$this->admin_model->get_election_information_and_feedback_for_winner();
        
        $this->load->view('EditElection',$query);
        
    }
    public function EditElection($pos) {
        
        $this->load->model('election_model');
        $this->load->model('admin_model');
        $rstartDate= $this->input->post('registration_start');
        $rendDate= $this->input->post('registration_over');
        $estartDate= $this->input->post('election_start');
        $eendDate= $this->input->post('election_over');
        $dateFormat='/^(19|20)\d\d[\-\/.](0[1-9]|1[012])[\-\/.](0[1-9]|[12][0-9]|3[01])$/';
        if(!preg_match($dateFormat, $rstartDate)||!preg_match($dateFormat, $rendDate)||!preg_match($dateFormat, $estartDate)||!preg_match($dateFormat,$eendDate))
        {
            
            $_POST['position']= urldecode($pos);
            $query=$this->admin_model->get_election_information_and_feedback_for_winner();
            $this->load->view('EditElection',$query);
            echo "<script>alert('Unable to add please check your date formats ');</script>";
            
        }
        else
        {
        if($this->admin_model->modify_election(urldecode($pos)))
        {
             $emailVoter=$this->admin_model->get_all_users();
             
        $this->load->model('email_model');
        
        for($i=0;$i<count($emailVoter['email']);$i++){            
                $this->email_model->sendMail($emailVoter['email'][$i],"Election Modified","Please note the changes to the following election:".$pos);
        }
            $query=$this->election_model->get_all_up_coming_elections();
            $this->load->view('ElectionManager',$query);
        }
        else{
            
            $query=$this->election_model->get_all_up_coming_elections();
            $this->load->view('ElectionManager',$query);
            echo "<script>alert('Unable to modify');</script>";
            
        }
        }
        
        
       
   
       
    }
    
    public function removeElection($pos) {
        
        $this->load->model('election_model');
        $this->load->model('admin_model');
        if($this->admin_model->remove_election(urldecode($pos)))
        {
            $query=$this->election_model->get_all_up_coming_elections();
            $this->load->view('ElectionManager',$query);
        }
        else{
            
            $query=$this->election_model->get_all_up_coming_elections();
            $this->load->view('ElectionManager',$query);
           echo "<script>alert('Unable to remove please try again');</script>";
            
        }
        
    }
    
public function sendReminders($pos) {
        
        $this->load->model('election_model');
        $this->load->model('admin_model');
        $this->load->model('email_model');
        $position=urldecode($pos);
        date_default_timezone_set('America/Chicago');
        $result = $this->election_model->get_reminder_sent_date($position);
        
        //false means that no email has ever been sent so send one
        if($result['reminder'] === FALSE){
        $emailList = $this->admin_model->get_all_voters_that_havent_voted($position);
            foreach ($emailList['email'] as $email)
            {
                
                $this->email_model->sendMail($email,"Please Vote", "The election :".$position." has started. Login at election.dragonbytelabs.com to vote" );
            }
            
            $this->election_model->set_reminder_sent_date($position);
            
               echo '<script> alert("Reminders have been sent");</script>';
        }
        else {
        $currentDate = new DateTime(date("Y-m-d H:i:s"));                   
        $lastReminderDate = new DateTime($result['reminder']);
        $interval = $currentDate->diff($lastReminderDate); //subtract $lastReminderDate from $currentDate
        
        if($interval->h <24 ){
            echo '<script> alert("Reminders have already been sent please try again later");</script>';
        
        }
        else{
            
            $emailList = $this->admin_model->get_all_voters_that_havent_voted($position);
            foreach ($emailList['email'] as $email)
            {
                $this->email_model->sendMail($email,"Please Vote", "The election :".$position." has started. Login at election.dragonbytelabs.com to vote" );
            }
        $this->election_model->set_reminder_sent_date($position);
        
              echo '<script> alert("Reminders have been sent");</script>';
        }
        
        }                                                        

        $query=$this->election_model->get_all_current_elections();
        $this->load->view('CurrentElections',$query);
              
    }
    
    
    public function declareElection($pos) {
        
        $this->load->helper('url');
        $this->load->model('election_model');
        $this->load->model('admin_model');
        if($this->admin_model->finalize_election_results(urldecode($pos)))
        {
            $query=$this->election_model->get_all_finished_elections();
            $this->load->view('CompletedElections',$query);

        }
        else
        {
            $query=$this->election_model->get_all_up_coming_elections();
            $this->load->view('ElectionManager',$query);
            echo "<script>alert('Unable to declare please try again');</script>";

        }
        
    }
    
    
    public function loadDeclareElection($pos) {
        
        $this->load->model('admin_model');
        $_POST['position']= urldecode($pos);
        $query=$this->admin_model->get_election_information_and_feedback_for_winner();
        $this->load->view('DeclareElection',$query);

        
        
    }
    public function loadCompletedElections() {
        
        $this->load->helper('url');
        $this->load->model('election_model');
        $query=$this->election_model->get_all_finished_elections();
        $this->load->view('CompletedElections',$query);
        
        
    }
    
    public function loadElectionManager() {
        
        $this->load->helper('url');
        $this->load->model('election_model');
        $query=$this->election_model->get_all_up_coming_elections();
        $this->load->view('ElectionManager',$query);
        
        
    }
    
    public function EMgrResults() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('election_model');
        //will have to change this to get_finished_election_results once the query is fixed
        $query = $this->election_model->get_all_elections();//get_all_finished_finalized_elections();
        
        $this->load->view('EMgrVresults', $query);
        
    }
    
    public function viewEmgrResults(){
        $this->load->helper('url');
        
        $this->load->model('election_model');
        
        if(isset($_POST['position'])){
            $query = $this->election_model->view_election_votes($_POST['position']);
            $stats=$this->election_model->get_hourly_results($_POST['position']);
            
            
            $this->load->library('gcharts');
            
            $this->gcharts->load('ColumnChart');
            
            $this->gcharts->DataTable('stats')->addColumn('string', 'Interval', 'count');
            $this->gcharts->DataTable('stats')->addColumn('number', 'voteCount', 'vcount');
            for($i=0;$i<sizeof($stats);$i++){
                $this->gcharts->DataTable('stats')->addRow(array(" ".$i,
                                                                 $stats[$i]
                                                                 ));
            }
            
            $config = array(
                            'title' => 'stats'
                            );
            
            $this->gcharts->ColumnChart('stats')->setConfig($config);
            
            
            $this->load->view('EmgrResults', $query);
        }
    }

    

    

    
}

?>
