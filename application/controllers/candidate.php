<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Candidate extends CI_Controller {

    //default load the candidate elections page and fills with the current elections
    public function index() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('election_model');
        $query = $this->election_model->get_current_eligible_elections($this->session->userdata('email'));
        
        $this->load->view('CandiElections', $query);   
    }


    //the controller that handles the load update profile    
    public function CandiCreateProfile() {
        $this->load->helper('url');
        //display the login form
        $voterEmail = $this->session->userdata('email');
        $this->load->model('candidate_model');
        $query = $this->candidate_model->get_candidate_info($voterEmail);
        $this->load->view('CandiCreateProfile',$query);
    }


    //the controller that handles update profile   
    public function CandiCreateProfilePost() {
        $this->load->helper('url');
        //display the login form
        
            if (isset($_POST['bio']) || isset($_POST['held_positions']))
            {

                    $bio = array(
                    'bio' => $_POST['bio'],
                    'held_positions' => $_POST['held_positions']
                    );
                    $email = $this->session->userdata('email');
                     
                    $this->load->model('candidate_model');
                    $query = $this->candidate_model->update_bio_held_positions($email,$bio);
            
                    $this->load->view('Candisuccess');
            }
            else 
            {
                 echo "Please submit the form."; 
            }
    }





    //the controller that handles the load create profile page    
    public function CandiViewProfile() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('candidate_model');
        $voterEmail = $this->session->userdata('email');
        $query = $this->candidate_model->get_candidate_info($voterEmail);
        $this->load->view('ViewProfilec',$query);
        
    }


    //the controller that handles the edit profile
    public function CandiEditProfile() {
        $this->load->helper('url');
        $voterEmail = $this->session->userdata('email');
        $this->load->model('candidate_model');
        $query = $this->candidate_model->view_candidate_info($voterEmail);
        $this->load->view('CandiEditProfile',$query);
        
    }


    //the controller that handles to the view the profile of all candidates
    public function CandiProfiles() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('candidate_model');
        $query = $this->candidate_model->get_all_candidates();
        $this->load->view('profilesCandi', $query);      
        
    }


    //the controller that handles the profiles of candidates  
    public function CandiProfilesMore() {
        $this->load->helper('url');
        //display the login form

            if (isset($_POST['last_name'])) 
            {
                
                $email =  $_POST['email'];
                
            }

                $this->load->model('candidate_model');
                $query = $this->candidate_model->get_candidate_info($email[0]);
                $this->load->view('ViewProfilec', $query);

    }



    //the controller that handles voter election results page   
    public function CandiElectionResults() {
        $this->load->helper('url');
        //display the login form
        $this->load->model('election_model');
        //will have to change this to get_finished_election_results once the query is fixed
        $query = $this->election_model->get_all_elections();//get_all_finished_finalized_elections();
        $this->load->view('CandiElectionResults', $query);

    }

    public function viewCandiResults(){
           $this->load->helper('url');
        //display the login form
        $this->load->model('election_model');
        
        if(isset($_POST['position'])){
         $query = $this->election_model->view_election_votes($_POST['position']);
            $stats=$this->election_model->get_hourly_results($_POST['position']);
            
            $this->load->library('gcharts');
            
            $this->gcharts->load('ColumnChart');
            
            $this->gcharts->DataTable('stats')->addColumn('string', 'Interval', 'count');
            $this->gcharts->DataTable('stats')->addColumn('number', 'voteCount', 'vcount');
            for($i=0;$i<sizeof($stats);$i++){
                $this->gcharts->DataTable('stats')->addRow(array(" ".$i,
                                                                 $stats[$i]
                                                                 ));
            }
            
            
            $config = array(
                            'title' => 'stats'
                            );
            
            $this->gcharts->ColumnChart('stats')->setConfig($config);
            
            $this->load->view('CandiViewResults', $query);
        }
    }


    //the controller that handles voter vote page  
    public function CandiVote() {
        $this->load->helper('url');
        //display the login form
            if (isset($_POST['position'])) { 
            $position =  $_POST['position'];

            $this->load->model('election_model');
            $query = $this->election_model->get_candidates_for_election($position);
            $this->load->view('CandiVote', $query);
            }

            else 
            {
                 echo "Please submit the form."; 
            }
    }


    //the controller that sumbit 
    public function submitVote() {
        $this->load->helper('url');
        //display the login form
        
            if (isset($_POST['email'])) 
            {
            $candemail =  $_POST['email'];
                if ($candemail=="")
                {
                    //echo "writein";
                    //$this->load->model('election_model');
                    //$query = $this->election_model->get_candidates_for_election($position);
                     $position =  $_POST['position'][0];

                    $this->load->model('election_model');
                    $query = $this->election_model->get_candidates_for_election($position);
            
                    $this->load->view('CandiWritein', $query);   
                    }
            

                else
                    {   
                    $position =  $_POST['position'][0];
                    $voterEmail = $this->session->userdata('email');

            

                    $this->load->model('election_model');
            
                    //echo " it's a ",$position;
                    //echo "email is",$voterEmail;
                    $query = $this->election_model->vote_for_candidate($voterEmail,$position,$candemail);
                        $receipt = $this->election_model->get_voter_id($voterEmail,$position);
                        
                        $this->load->model('email_model');
                        $this->email_model->sendMail($voterEmail,'Vote Confirmation number and receipt','Dear Voter, \n Yout vote confirmation mumber is :'.$receipt['id'].'for the election:'.$position.'\n Thank you for voting.');
                    //post to twitter
                    //$tweet = $position;
                    //$this->load->model('twitter_model');
                     //$this->twitter_model->publish_tweets($voterEmail, $position);
                    

                    $this->load->view('Candisuccesspage', $receipt);
                    //echo "good";
                    }
            }
            else 
            {
                 echo "Please submit the form."; 
            }
    }





            //the controller that that write in votes 
    public function Votesuccesswritein() {
        $this->load->helper('url');
        //display the login form
        
            if (isset($_POST['email'])) 
            {
            $candemail =  $_POST['email'];
            $position =  $_POST['position'][0];
            $voterEmail = $this->session->userdata('email');
            $voterEmail = $this->session->userdata('email');
            

            $this->load->model('election_model');
            $query = $this->election_model->write_in_candidate($voterEmail,$position,$candemail);

                $receipt = $this->election_model->get_voter_id($voterEmail,$position);
                
                $this->load->model('email_model');
                $this->email_model->sendMail($voterEmail,'Vote Confirmation number and receipt','Dear Voter, \n Yout vote confirmation mumber is :'.$receipt['id'].'for the election:'.$position.'\n Thank you for voting.');
            //post to twitter
            //$tweet = $position;
            //$this->load->model('twitter_model');
            //$this->twitter_model->publish_tweets($voterEmail, $position);
            
            $this->load->view('Candisuccesspage', $receipt);
            }
            else 
            {
                 echo "Please submit THE form."; 
            }
    }


     //the controller that handles reelection 
    public function ReReElection() {
        $this->load->helper('url');
        //display the login form
        $this->load->view('ReReElection');

        $this->load->model('election_model');
        $query = $this->election_model->view_election_votes();
    }

    //the controller that handles apply voter/cand   
        public function Candiapplyvoter() {
        $this->load->helper('url');
        //display the login form
        $voterEmail = $this->session->userdata('email');
        $this->load->model('membership_model');
        $query = $this->membership_model->get_current_elections_for_registration();
        $this->load->view('Candiapplyvoter',$query);
    }


            //the controller that handles apply voter/cand   
        public function Candiapplyvotersumbit() 
        {
            $this->load->helper('url');
            //display the login form
            if (isset($_POST['election'])) 
            {
                    $position = $_POST['election'];
                    $email = $this->session->userdata('email');
                    $this->load->model('membership_model');
                  $this->membership_model->apply_to_be_voter($email,$position);
            
                    $this->load->view('Candisuccess');
            }
            else 
            {
                 echo "Please submit the form."; 
            }

        }



                //the controller that handles apply voter/cand   
        public function Candiapplycandi() {
        $this->load->helper('url');
        //display the login form
        $voterEmail = $this->session->userdata('email');
        $this->load->model('membership_model');
        $query = $this->membership_model->get_current_elections_for_registration();
        $this->load->view('Candiapplycandi',$query);
    }


            //the controller that handles apply voter/cand   
        public function Candiapplycandisumbit() 
        {
            $this->load->helper('url');
            //display the login form
            if (isset($_POST['election'])) 
            {
                    $position = $_POST['election'];
                    $email = $this->session->userdata('email');
                    $this->load->model('membership_model');
                    $query = $this->membership_model->apply_to_be_candidate($email,$position);
            
                    $this->load->view('Candisuccess');
            }
            else 
            {
                 echo "Please submit the form."; 
            }

        }

   //the controller that handles the load update profile    
    public function viewChangePassword() {
        $this->load->helper('url');
  
        $this->load->view('CandiChangePassword');
    }


         public function changePassword() {
        $this->load->helper('url');
        //display the login form
       
        
            if (isset($_POST['oldPassword']) && isset($_POST['newPassword'])&& isset($_POST['cNewPassword'])) 
            {
                 if($_POST['newPassword'] === $_POST['cNewPassword']){
                
                    $email = $this->session->userdata('email');
                     
                    $this->load->model('membership_model');
                    $query = $this->membership_model->change_password($email,$_POST['oldPassword'],$_POST['newPassword']);
            if($query)echo '<script>alert("Successful");</script>';//$this->load->view('Votersuccesspage');
            else echo "Please make sure you have entered the correct old password.";
                 }
                 else {
                      echo "Please make sure your new password and your password confirmation are the same.";
                 }
            }
            else 
            {
                 echo "Please submit the form."; 
            }
    }



}






?>
